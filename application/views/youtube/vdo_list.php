<div class="youtube-container">
    <?php
        $len    = count($rs['data']['vdo']);
        for( $i=0;  $i<$len;  $i++ ){
            $item = $rs['data']['vdo'][$i];
        ?>
            <div class="row">
                <div class="col-xs-12">
                    
                    <div class="thumbnail">
                        <?php /** Destop size */?>
                        <iframe src="https://www.youtube.com/embed/<?php echo $item['id'];?>" 
                                style="width:100%; height:auto; min-height: 200px"
                                class="hidden-sm hidden-xs"
                                frameborder="0" allowfullscreen></iframe>
                        
                        <?php /** Mobile size */?>
                        <iframe src="https://www.youtube.com/embed/<?php echo $item['id'];?>" 
                                style="width:100%; height:auto; min-height: 300px"
                                class="hidden-lg hidden-md"
                                frameborder="0" allowfullscreen></iframe>
                        
                        <p class="p-first"><?php echo $item['title'];?></p>
                        <?php
                            if($item['description']){
                            ?>
                                <p><?php echo $item['description'];?></p>
                            <?php
                            }
                        ?>
                    </div>
                    
                </div>
            </div>
        <?php
        }
    ?>
</div>