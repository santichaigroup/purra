<!DOCTYPE html>
<html lang="<?php echo getLang();?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <?php
        $socials            = getPurra()->social(true);
        $shareFacebook      = @$socials->data->bottom->facebook[0] ? $socials->data->bottom->facebook[0]    : "";
        /*
        $FBShare_picture    = base_url('public/image/thumb-share-1200x630.jpg');
        $FBShare_caption    = "PURRA : feel so good : น้ำแร่ธรรมชาติ 100% ตราเพอร์ร่า";
        $FBShare_description= 'น้ำแร่ธรรมชาติแหล่งพระงาม ดื่มให้กับทุกความรู้สึก น้ำแร่ธรรมชาติ 100% ตราเพอร์ร่า "Feel so good"';
         * 
         */
        $FBShare_picture    = "";
        $FBShare_caption    = "";
        $FBShare_description= "";
        if( @$shareFacebook->logo ){
            $FBShare_picture    = $shareFacebook->logo;
        }
        if( @$shareFacebook->name ){
            $FBShare_caption    = $shareFacebook->name;
        }
        if( @$shareFacebook->description ){
            $FBShare_description    = $shareFacebook->description;
        }

        if( @$_META_OG ){
            echo $_META_OG;
        }else{
            $socials            = getPurra()->social(true);
            $social_facebook    = $socials->data->bottom->facebook[0];
            ?>
                <meta property="og:url"           content="<?php echo site_url();?>" />
                <meta property="og:type"          content="article" />
                <meta property="og:title"         content='<?php echo @$social_facebook->name ? $social_facebook->name : "";?>' />
                <meta property="og:description"   content='<?php echo @$social_facebook->description ? $social_facebook->description : "";?>' />
                <meta property="og:image"         content="<?php echo @$social_facebook->logo ? $social_facebook->logo : "";?>" />
            <?php
        }

        $seo            = getPurra()->seo();
        $seo            = @json_decode($seo);
        $title          = @$seo->data->seo[0]->title ? $seo->data->seo[0]->title : "";
        $keyword        = @$seo->data->seo[0]->keyword ? $seo->data->seo[0]->keyword : "";
        $description    = @$seo->data->seo[0]->description ? $seo->data->seo[0]->description : "";
        $googleAnalytic = @$seo->data->seo[0]->analytics ? $seo->data->seo[0]->analytics : "";
        $googleAnalytic = str_replace("\\n", "", str_replace("\\r\\n", "", $googleAnalytic));
        if( @$_TITLE ){
            $title = $_TITLE;
        }
        if( @$_KEYWORD ){
            $keyword = $_KEYWORD;
        }
        //$description    = @$_DESC ? str_replace("&nbsp;", "", $_DESC) : "";
        if( @$_DESCRIPTION ){
            $description = $_DESCRIPTION;
        }
        $_ENV['meta_description'] = $description;
    ?>


    <title><?php echo $title;?></title>
    <meta name="keywords" content="<?php echo $keyword;?>" />
    <meta name="description" content="<?php echo $description;?>" />
    <base href="<?php echo base_url('public');?>/" />

    <link rel="shortcut icon" href="assets/images/favicon.ico" />
    <link rel="canonical" href="http://www.singha.com/purra/">

    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="bower_components/bootstrap-select/dist/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="bower_components/OwlCarousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="bower_components/OwlCarousel2/dist/assets/owl.theme.default.min.css">
    <!--<link rel="stylesheet" href="js/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="js/owl-carousel/owl.theme.css">-->
    <link rel="stylesheet" href="bower_components/fancyBox/source/jquery.fancybox.css" />
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/css/animations-ie-fix.css">
        <script src="bower_components/html5shiv/dist/html5shiv.min.js"></script>
        <script src="bower_components/Respond/dest/respond.min.js"></script>
    <![endif]-->


    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <style>
        .form-search{
            position: fixed;
        }
        #fb-root {
          display: none;
        }


        #tab_facebook .fb_iframe_widget, 
        #tab_facebook .fb_iframe_widget span, 
        #tab_facebook .fb_iframe_widget span iframe[style] {
                width: 100% !important;
        }
        #tab_facebook{
            text-align: center;
        }
        @media (max-width: 1023px){
            #tab_facebook .fb_iframe_widget, 
            #tab_facebook .fb_iframe_widget span, 
            #tab_facebook .fb_iframe_widget span iframe[style] {
                    width: 100% !important;
            }
            #tab_facebook .fb_iframe_widget iframe[style]{
                    padding: 0 15%;
            }
        }
        @media (max-width: 1023px) and (min-width: 960px){
            #tab_facebook .fb_iframe_widget iframe[style]{
                    padding: 0 28%;
            }
        }



        /* Social button */
        .btn-share{
            float: left;
            min-width: 95px;
            width: auto;
        }
        .social-share-container{
            float: right; 
            width: 200px;
        }
        .btn-share.twitter{
            min-width: 65px;
            position: relative;
            top: 1px;
        }
        .btn-share.line{
            min-width: 70px;
            margin-left: 10px;
        }
        .btn-share.line > a{
            position: relative;
            top: -2px;
        }
        .btn-share.facebook{ min-width: 125px; }
        .clear{ clear: both; }

        /* Youtube, IG Tab */
        .youtube-container .thumbnail,
        .instagram-container .thumbnail
        { margin: 6px; }
        .youtube-container .thumbnail > p,
        .instagram-container .thumbnail > p
        { margin: 5px; }
        .youtube-container .thumbnail > p.p-first,
        .instagram-container .thumbnail > p.p-first
        { margin-top: 10px; font-weight: bold; }
        .youtube-container .row,
        .instagram-container .row
        { margin: 0px; }
        .youtube-container .row > .col-xs-12,
        .instagram-container .row > .col-xs-12
        { padding: 1px; }


        /* Replace Style */
        .sidebar .tab-content > .tab-pane{
            min-height: 430px;
            max-height: 500px;
            overflow: auto;
        }
        .fb-page iframe {
            height: 100% !important;
        }
        body{ min-width: 0px !important; }
            .box-shadow{text-align:center !important;}
        .list-items > li{ display: block !important; }

        /* page: Home, section: lifestyle, Promotion&Activity */
        /*
        ul#owl-lifestyle div.owl-stage,
        ul#owl-promotion div.owl-stage{
            width: 100% !important;
        }
        */

        .flexslider .flex-direction-nav .flex-prev,
        .flexslider .flex-direction-nav .flex-next{
            padding-top: 10px;
        }
        
        
        
        .footer label {
            margin: 3px 10px 3px 0;
        }
        div.newsleter{
            padding-right: 0px;
        }
        
        @media (max-width: 1366px){
            .move_up {
                right: -30px;
            }  
        }
        
        @media (max-width: 800px){
            .move_up {
                right: 14px;
            }
        }
        
        

    </style>

    <script>



        function loadFacebookFanpagePlugin(jQueryObj, width, height){
            $.post( "<?php echo _site_url("view/facebookFanpagePlugin");?>", {
                width   : width,
                height  : height
            },function(html){
                jQueryObj.html("width: "+width+ ", height: "+height +"<br />"+html);
                jQueryObj.html(html);
                try{FB.XFBML.parse();}
                catch(e){}
            });
        }

        function shareFaceBook(){
            FB.ui({
                method: 'share',
                href: '<?php echo site_url();?>',
                picture     : '<?php echo $FBShare_picture;?>',
                caption     : '<?php echo $FBShare_caption;?>',
                description : '<?php echo $FBShare_description;?>'
            }, function(response){});
        }

        function shareFacebookViaPage( href, pictureUrl, caption, desc ){
            FB.ui({
                method: 'share',
                href: href,
                picture     : '<?php echo _site_url('image/ratio/?width=160&height=160')?>&file='+ pictureUrl,
                caption     : caption,
                description : desc
            }, function(response){});
        }
        function onSearch(){
            if( $('.form-search input.form-control').val()=="" ){
                return false;
            }
            return true;
        }
        function onSearchInPage(){
            if( $('.form-search-inpage input.form-control').val()=="" ){
                return false;
            }
            return true;
        }

    </script>

    <?php echo $googleAnalytic;?>

</head>
<?php
    $background = getPurra()->site_background();
    $background = @json_decode($background);
    $controller     = $this->uri->segment(2);
    if( $controller=='' ){
        $controller = "home";
    }
    
    $bodyBackground = "";
    
    $isMobile       = $mobileDetect->isMobile();
    $isTable        = $mobileDetect->isTablet();
    
    switch($controller){
        /*
        case "home" : {
            if( $isMobile ){
                $bodyBackground = @$background->data->home[0]->mobile ? $background->data->home[0]->mobile : "" ;
            }else{
                $bodyBackground = @$background->data->home[0]->desktop ? $background->data->home[0]->desktop : "" ;
            }
            
            if( $bodyBackground!="" ){
                $bodyBackground = "background: #fff url({$bodyBackground}) no-repeat top center";
            }
        } break;
         * 
         */
    
        
        case "lifestyle" : {
            $bodyBackground     = $background->data->lifestyle[0]->desktop;
            if( $isMobile  &&  @$background->data->lifestyle[0]->mobile ){
                $bodyBackground = $background->data->lifestyle[0]->mobile;
            }
            else if( $isTable  &&  @$background->data->lifestyle[0]->tablet ){
                $bodyBackground = $background->data->lifestyle[0]->tablet;
            }
            
            if( $bodyBackground!="" ){
                $bodyBackground = "background: #e1f0f7 url({$bodyBackground}) no-repeat top center";
            }
            
        } break;
        
        case "promotion" : {
            $method = $this->uri->segment(3);
            switch ($method){
                case "allPromotion" : {
                    $bodyBackground     = $background->data->promotion[0]->desktop;
                    if( $isMobile  &&  @$background->data->promotion[0]->mobile ){
                        $bodyBackground = $background->data->promotion[0]->mobile;
                    }
                    else if( $isTable  &&  @$background->data->promotion[0]->tablet ){
                        $bodyBackground = $background->data->promotion[0]->tablet;
                    }
                }break;
                case "allActivities" : {
                    $bodyBackground     = $background->data->activity[0]->desktop;
                    if( $isMobile  &&  @$background->data->activity[0]->mobile ){
                        $bodyBackground = $background->data->activity[0]->mobile;
                    }
                    else if( $isTable  &&  @$background->data->activity[0]->tablet ){
                        $bodyBackground = $background->data->activity[0]->tablet;
                    }
                } break;
                default : {
                    $bodyBackground     = $background->data->promotion_activity[0]->desktop;
                    if( $isMobile  &&  @$background->data->promotion_activity[0]->mobile ){
                        $bodyBackground = $background->data->promotion_activity[0]->mobile;
                    }
                    else if( $isTable  &&  @$background->data->promotion_activity[0]->tablet ){
                        $bodyBackground = $background->data->promotion_activity[0]->tablet;
                    }
                } break;
            }
            if( $bodyBackground!="" ){
                $bodyBackground = "background: #e1f0f7 url({$bodyBackground}) no-repeat top center";
            }
        } break;
    }
    
    
?>
<body class="<?php echo $body_class;?>" style="<?php echo $bodyBackground;?>">
	<div id="fb-root"></div>
	<script>
            window.fbAsyncInit = function() {
                FB.init({
                  appId      : '<?php echo FB_APP_ID;?>',
                  xfbml      : true,
                  status     : true,
                  version    : 'v2.5'
                });
            };
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=<?php echo FB_APP_ID;?>";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

	<?php $this->load->view("include/header"); ?>
        
        
        
        
        
        <?php
            /* CSS Background highlight, Product banner,..... */ 
            $controller = $this->uri->segment(2, "home");
            switch( $controller ){
                case "home" : {
                    $highlight = getPurra()->highlight();
                    $highlight = @json_decode($highlight);
                    $highlightBg = "";
                    if($isMobile ){
                        $highlightBg = @$highlight->data->highlight[0]->banner_mobile ? $highlight->data->highlight[0]->banner_mobile : "";
                    }else{
                        $highlightBg = @$highlight->data->highlight[0]->banner_desktop ? $highlight->data->highlight[0]->banner_desktop : "";
                    }
                    
                    if( $highlightBg!="" ){
                    ?>
                        <style>
                            body.home .mainContent .banner {
                                background: #fff url(<?php echo $highlightBg;?>) no-repeat center !important;
                            } 
                        </style>
                    <?php
                    }
                } break;
                
                /*
                case "promotion" : {
                    $method             = $this->uri->segment(3);
                    $ctrlBackgroundObj  = null;
                    switch($method){
                        // Activity
                        case "allActivities" : {
                            $ctrlBackgroundObj  = getPurra()->activityBanner();
                            $ctrlBackgroundObj  = @json_decode($ctrlBackgroundObj);
                        } break;
                        
                    
                        // Promotion&Activity & Promotions
                        case "allPromotion" : 
                        default : {
                            $ctrlBackgroundObj  = getPurra()->promotionBanner();
                            $ctrlBackgroundObj  = @json_decode($ctrlBackgroundObj);
                        } break;
                    }
                    
                    $ctrlBackground     = "";
                    if( $ctrlBackgroundObj ){
                        if($isMobile ){
                            $ctrlBackground = @$ctrlBackgroundObj->data->banners[0]->banner_mobile ? 
                                                    $ctrlBackgroundObj->data->banners[0]->banner_mobile : 
                                                    "" ;
                        }else{
                            $ctrlBackground = @$ctrlBackgroundObj->data->banners[0]->banner_desktop ? 
                                                    $ctrlBackgroundObj->data->banners[0]->banner_desktop : 
                                                    "" ;
                        }
                        if($ctrlBackground!=""){
                        ?>
                            <style>
                                body.promotion .mainContent {
                                    background: url(<?php echo $ctrlBackground;?>) no-repeat top center !important;
                                    background-position-y: -175px !important;
                                } 
                            </style>
                        <?php
                        }
                    }
                    
                } break;
                 * 
                 */
            }
        ?>
        
		
	<?php echo $_BODY;?>


	<?php $this->load->view("include/footer");?>
                   
                        
                     
        <?php
            $_route = $this->uri->segment(2);
            $_route = strtolower($_route);
            if( $_route=='home'  ||  $_route=='' ){
                $specialEvents       = getPurra()->specialEvent();
                $specialEvents       = @json_decode($specialEvents);
                if( $specialEvents  
                    && @$specialEvents->data->content
                    && count($specialEvents->data->content)>0
                    && $specialEvents->data->content[0]->visible=='1' )
                {
                    $specialEvent   = $specialEvents->data->content[0];
                    $image          = $specialEvent->photo_desktop;
                    if( $mobileDetect->isMobile() ){
                        $image          = $specialEvent->photo_mobile;
                    }else if( $mobileDetect->isTablet() ){
                        $image          = $specialEvent->photo_tablet;
                    }
                ?>
                    <div class="modal fade popup" id="modal-event-dialog" 
                        tabindex="-1" role="dialog" 
                        aria-labelledby="mySmallModalLabel" 
                        aria-hidden="true"
                        style="min-height: 800px;
                                height: auto;">
                       <div class="modal-dialog"
                            style="max-width: 800px;">
                           <div class="modal-content">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                               <div class="modal-bordered">
                                   <div class="modal-body" 
                                        style="padding:0px;">
                                       <span class="modal-body-content" style="overflow: hidden;">
                                           <a href="<?php echo $specialEvent->url; ?> " target="_blank">
                                               <img src="<?php echo $image;?>" alt="<?php $specialEvent->alt;?>" width="100%" />
                                           </a>
                                       </span>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>  
                <?php
                }
            }
        ?>                
                      
                        
                        
        

	<!-- javascript -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="bower_components/placeholders/dist/placeholders.min.js"></script>
        <script src="bower_components/OwlCarousel2/dist/owl.carousel.min.js"></script>
        <!--<script src="js/owl-carousel/owl.carousel.min.js"></script>-->
        <script src="bower_components/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script src="bower_components/scrollReveal.js/scrollReveal.js"></script>
        <script src="bower_components/mobile-detect/mobile-detect.min.js"></script>
        <script src="assets/js/jquery.validate.min.js"></script>
        <script src="assets/js/jquery.mobile.custom.min.js"></script>
        <script src="assets/js/carousel.js"></script>
        <script src="assets/js/main.js"></script>
        <script>window.twttr = (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
          t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function(f) {
          t._e.push(f);
        };

        return t;
      }(document, "script", "twitter-wjs"));</script>
      
        <script>
            <?php
                switch( $this->uri->segment(2) ){
                    case "search" : {
                    ?>
                        $("#close-search").hide();
                        $("#open-search").click(function() {
                            $(".search-results .input-group input.form-control").focus();
                        }); 
                    <?php
                    } break;
                    default : {
                    ?>
                        $("#close-search").hide();
                        $("#open-search").click(function() {
                                    $("#open-search").toggleClass('active');
                                    $(".form-search").slideToggle(300);
                        });
                    <?php
                    } break;
                }
            ?>
            
            <?php
                if( ( $_route=='home'  ||  $_route=='' )
                    && $specialEvents  
                    && @$specialEvents->data->content
                    && count($specialEvents->data->content)>0
                    && $specialEvents->data->content[0]->visible=='1')
                {
                ?>
                    $(function(){
                        $("#modal-event-dialog").on('show.bs.modal', function(){
                           initSpecialEvent(); 
                        });
                        $("#modal-event-dialog").modal('show');
                        $(window).resize(function(){
                            initSpecialEvent();
                        });
                    });
                    function initSpecialEvent(){
                        //$("#modal-event-dialog .modal-body").css('min-height', '500px');
                        $("#modal-event-dialog .modal-body").css('max-height', 'auto');
                        $("#modal-event-dialog .modal-body").css('overflow-y', 'hidden');
                    }
                <?php
                }
            ?>
            
            
        </script>
        
</body>
</html>
