<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="47; url=<?php echo _site_url("home");?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>PURRA : feel so good : น้ำแร่ธรรมชาติ 100% ตราเพอร์ร่า</title>
    
    <base href="<?php echo base_url('public');?>/" />
    
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/intro.css">


    <script type="text/javascript" id="www-widgetapi-script" src="https://s.ytimg.com/yts/jsbin/www-widgetapi-vfloT07bb/www-widgetapi.js" async=""></script><script src="https://www.youtube.com/iframe_api"></script></head>
    <body class="intro">


    <div class="logo-purra"><img src="assets/images/logo_purra.png"></div>
    <a class="enter" href="<?php echo _site_url("home");?>"></a>
    <div id="background-video" class="background-video loaded"><div id="ytplayer-container1450598238229" class="ytplayer-container background">                                    <iframe id="YTPlayer-ID-1450598238229" class="ytplayer-player" style="width: 1436px; height: 808px; left: 0px; top: 0px;" frameborder="0" allowfullscreen="1" title="YouTube video player" width="1436" height="808" src="https://www.youtube.com/embed/YikcOPwMNhk?modestbranding=1&amp;autoplay=0&amp;controls=1&amp;showinfo=0&amp;wmode=transparent&amp;branding=0&amp;rel=0&amp;autohide=0&amp;enablejsapi=1"></iframe>                                    </div>                                    <div id="ytplayer-shield"></div></div>


    <!-- javascript -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="assets/js/jquery.youtubebackground.js"></script>
    <script>
    $(function(){
        $('#background-video').YTPlayer({
            videoId: 'YikcOPwMNhk',
            mute:false
        });
    });
    </script>



    </body>
</html>