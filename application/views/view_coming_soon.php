<div class="mainContent">

    <?php $this->load->view("include/share_social");?>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">	
                <div class="cover-container">
                    <div class="coming-soon">
                        <img src="assets/images/coming_soon.png">
                        <a href="javascript:history.back();" class="btn-back"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>