<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="47; url=<?php echo _site_url("home");?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <meta property="og:url"         content="<?php echo _site_url("")?>" /> 
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Purra Feel so good: 100% natural mineral water" />
    <meta property="og:description"   content="น้ำแร่ธรรมชาติแหล่งพระงาม ดื่มให้กับทุกความรู้สึก น้ำแร่ธรรมชาติ 100% ตราเพอร์ร่า Feel so good " /> 
    <meta property="og:image"         content="<?php echo base_url('public/image/thumb-share-1200x630.jpg');?>" />

    
    
    <title>PURRA : feel so good : น้ำแร่ธรรมชาติ 100% ตราเพอร์ร่า</title>
    
    <base href="<?php echo base_url('public');?>/" />
    
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/intro.css">

	<body class="intro">

	<div class="logo-purra"><img src="assets/images/logo_purra.png"></div>
	<a class="enter" href="<?php echo _site_url("home");?>"></a>
	<div id="background-video" class="background-video" 
	data-property="{videoURL:'YikcOPwMNhk',
				containment:'self',
				autoplay: true,
               	optimizeDisplay: true,
               	showControls: true,
               	startAt: 0,
               	opacity: 1,	
               	addRaster: false}"></div>


	<!-- javascript -->
	<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- 	<script src="assets/js/jquery.youtubebackground.js"></script> -->
	<script src="jquery.mb.YTPlayer.min.js"></script>
	<script>
	$(function(){
	/*
	    $('#background-video').YTPlayer({
	        videoId: 'YikcOPwMNhk',
	        mute:false,
	        autoplay: 1,
	        autoPlay: true,
	        playerVars: { 
	        	autoplay: 1
	        }
	    });
	    */
	    
	    $('#background-video').YTPlayer();
	});
	</script>

    </body>
</html>