<div class="mainContent">


    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->
    

    <div class="container">

        <h1><?php echo langs('CONTACT US');?></h1>
        
        <ol class="breadcrumb">
            <li><a href="<?php echo _site_url("home");?>">Home</a></li>
            <li><a><?php echo langs('CONTACT US');?></a></li>
        </ol>

    	<div class="row">
        	<div class="col-md-6">
            
                <div class="box-shadow address">
                	<div class="row">
                        <div class="col-lg-5 col-md-4 col-xs-5">
                            <img src="assets/images/contact/logo_singha.png" class="logo">
                        </div>
                        <?php
                            $footer     = getPurra()->footer();
                            $footer     = json_decode($footer);
                            $contact    = $footer->code==200 ? $footer->data->contact : null;
                            $location   = $footer->code==200 ? $footer->data->location : null;
                            //$lat        = $location->latitude;
                            //$lng        = $location->longitude;
                            $lat        = '13.7912442'; //Boonrawd Trading Co.,LTD.
                            $lng        = '100.5112307';//Boonrawd Trading Co.,LTD.
                        ?>
                        <div class="col-lg-7 col-md-8 col-xs-7">
                            <h4><?php echo $contact ? $contact->company_name : "";?></h4>
                            <p><?php echo $contact ? $contact->company_address : "";?><br> 
                            โทร: <?php echo $contact ? $contact->company_phone : "";?><br> 
                            แฟกซ์: <?php echo $contact ? $contact->company_fax : "";?><br> 
                            อีเมล: callcenter@boonrawd.co.th
                            </p>
                        </div>
                    </div>
                </div>
                    
                <div class="box-shadow">
                    <div id="map-container">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3874.809780973862!2d100.51502870796509!3d13.790337699003878!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xe3b3dcc99520de96!2z4Lia4Li44LiN4Lij4Lit4LiU4LmA4LiX4Lij4LiU4LiU4Li04LmJ4LiH!5e0!3m2!1sth!2sth!4v1453562248679" 
                                width="100%" 
                                height="100%" 
                                frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>

                
            </div>
        	<div class="col-md-6 col-xs-12">
            
                <?php $this->load->view("contact/form"); ?>
                
            </div>
        </div>
        
        <div class="move_up"></div>
    </div><!-- /.container -->
    

    <div class="modal fade popup" id="contact-thank" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-bordered">
                	<img src="assets/images/contact/packshot.png" class="img-packshot">
                    <div class="modal-body">
                        <h3>ขอบคุณค่ะ</h3>
                        <p>ทางทีม เพอร์ร่า ได้รับข้อมูลของคุณเรียบร้อยแล้ว<br> และทางทีมจะติดต่อคุณกลับไปโดยเร็วค่ะ</p>
                        <img src="assets/images/contact/back_to_home.png" class="center-block" data-dismiss="modal">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /#contact-thank -->


</div>

<!--<script src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<script>
    /*
	function init_map() {
		var var_location = new google.maps.LatLng('13.7903377', '100.5150287');
		var var_mapoptions = {
			center: var_location,
			zoom: 17
	};
	var var_marker = new google.maps.Marker({
		position: var_location,
		map: var_map,
		title:"<?php echo $contact ? $contact->company_name : "";?>"});
	var var_map = new google.maps.Map(document.getElementById("map-container"),
		var_mapoptions);
		var_marker.setMap(var_map);	
	}
	google.maps.event.addDomListener(window, 'load', init_map);
        */
</script>