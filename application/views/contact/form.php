<div class="form-contact box-shadow">
    <form action="" method="post" name="FormContact" id="FormContact" novalidate="novalidate" onsubmit="return checkContactForm();">
        
        <?php
            $contact = getPurra()->getContact();
            $contact = @json_decode($contact);
            if($contact  &&  $contact->code==200){
            ?>
                <div class="form-group">
                    <select name="type" class="form-control" id="type" title="Select Contact Type*" data-rule-required="true" data-msg-required="กรุณาเลือกประเภท">
                        <?php
                            $len    = is_array($contact->data->type) ? count($contact->data->type) : 0 ;
                            for( $i=0;  $i<$len;  $i++ ){
                                $item = $contact->data->type[$i];
                            ?>
                                <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                            <?php
                            }
                        ?>
                    </select>
                </div>
            <?php
            }
        ?>
        
        
        <div class="form-group">
            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject*" maxlength="255" data-rule-required="true" data-msg-required="กรุณากรอกหัวข้อ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="name" id="name" placeholder="Name*" maxlength="100" data-rule-required="true" data-msg-required="กรุณากรอกชื่อ – นามสกุล">
        </div>
        <div class="form-group">
            <input type="email" class="form-control" name="email" id="email" placeholder="Email*" maxlength="255" data-rule-required="true" data-rule-email="true" data-msg-required="กรุณากรอกอีเมล์" data-msg-email="กรุณากรอกอีเมล์ให้ถูกต้อง">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone*" maxlength="10" data-rule-required="true" data-rule-number="true" data-msg-required="กรุณากรอกเบอร์โทรศัพท์มือถือ" data-msg-number="กรุณากรอกหมายเลขโทรศัพท์มือถือ 10 หลัก">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="message" id="message" placeholder="Message*" maxlength="255" data-rule-required="true" data-msg-required="กรุณากรอกข้อความที่ต้องการติดต่อ">
        </div>
        <div class="form-group captcha">
            <div class="row"
                 style="height: auto;
                        margin-left: 0px;
                        margin-right: 0px;">
<!--                <div class="col-xs-6">
                        <div class="img-captcha"><img src="assets/images/contact/captcha.jpg"></div>
                </div>
                <div class="col-xs-6">
                        <input type="text" class="form-control" name="code" maxlength="10" data-rule-required="true" data-msg-required="กรุณากรอกตัวอักษร">
                </div>-->
                <script src='https://www.google.com/recaptcha/api.js'></script>
                <div class="g-recaptcha" 
                     data-sitekey="<?php echo reCAPTCHA_KEY;?>"
                     data-size="normal"></div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
    </form>
</div>


<script>
    function checkContactForm(){
        if( $('#FormContact').valid() ){
            if( $('form[name=FormContact] #type').val()=="" ){
                $("#modal-dialog .modal-body-content").html('<p style="color:red;">กรุณาเลือกประเภท</p>');
                $("#modal-dialog").modal('show');
            }else{
                $('button[type="submit"]').attr('disabled','disabled');
                $.post("<?php echo _site_url("api/contact");?>", {
                    type    : $('form[name=FormContact] #type').val(),
                    subject : $('form[name=FormContact] #subject').val(),
                    name    : $('form[name=FormContact] #name').val(),
                    email   : $('form[name=FormContact] #email').val(),
                    phone   : $('form[name=FormContact] #phone').val(),
                    message : $('form[name=FormContact] #message').val(),
                    'g-recaptcha-response' : $('#g-recaptcha-response').val()
                }, function(data){
                    grecaptcha.reset();
                    data = $.parseJSON(data);
                    if( data.status==true ){
                        $('#contact-thank').modal('show');
                        $('form[name=FormContact] input').val('');
                    }else{
                        $("#modal-dialog .modal-body-content").html('<p style="color:red;">เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง</p>');
                        $("#modal-dialog").modal('show');
                    }
                    $('button[type="submit"]').removeAttr('disabled');
                });
                
            }
        }else{
            $("#modal-dialog .modal-body-content").html('<p style="color:red;">กรุณากรอกข้อมูลให้ครบถ้วน</p>');
            $("#modal-dialog").modal('show');
        }
        
        return false;
    }
</script>