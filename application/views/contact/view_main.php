<div class="mainContent">


    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->
    

    <div class="container">

        <h1><?php echo "CONTACT US" ; //langs('CONTACT US');?></h1>
        
        <ol class="breadcrumb">
            <li><a href="<?php echo _site_url("home");?>">Home</a></li>
            <li><a>Contact Us<?php //echo langs('CONTACT US');?></a></li>
        </ol>

    	<div class="row">
        	<div class="col-md-6">
            
                <div class="box-shadow address">
                	<div class="row">
                        <div class="col-lg-5 col-md-4 col-xs-5">
                            <img src="assets/images/contact/logo_singha.png" class="logo">
                        </div>
                        <?php
                            $footer     = getPurra()->footer();
                            $footer     = json_decode($footer);
                            $contact    = $footer->code==200 ? $footer->data->contact : null;
                            $location   = $footer->code==200 ? $footer->data->location : null;
                            //$lat        = $location->latitude;
                            //$lng        = $location->longitude;
                            
                            $getContactObj  = getPurra()->getContact();
                            $getContactObj  = @json_decode($getContactObj);
                            
                            $lat        = $getContactObj && @$getContactObj->data->location ? $getContactObj->data->location->lat : '13.791239'; //Boonrawd Trading Co.,LTD.
                            $lng        = $getContactObj && @$getContactObj->data->location ? $getContactObj->data->location->long : '100.515608'; //Boonrawd Trading Co.,LTD.
                        ?>
                        <div class="col-lg-7 col-md-8 col-xs-7" style="text-align: left;">
                            <h4><?php echo @$getContactObj->data->address->name ? $getContactObj->data->address->name : "";?></h4>
                            <p><?php echo @$getContactObj->data->address->address ? $getContactObj->data->address->address : "";?><br> 
                            โทร: <?php echo @$getContactObj->data->address->phone ? $getContactObj->data->address->phone : "";?><br> 
                            แฟกซ์: <?php echo @$getContactObj->data->address->fax ? $getContactObj->data->address->fax : "";?><br> 
                            อีเมล: <?php echo @$getContactObj->data->address->email ? $getContactObj->data->address->email : "";?>
                            </p>
                        </div>
                    </div>
                </div>
                    
                <div class="box-shadow">
                    <div id="map-container"></div>
                </div>

                
            </div>
        	<div class="col-md-6 col-xs-12">
            
                <?php $this->load->view("contact/form"); ?>
                
            </div>
        </div>
        
        <div class="move_up"></div>
    </div><!-- /.container -->
    

    <div class="modal fade popup" id="contact-thank" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-bordered">
                	<img src="assets/images/contact/packshot.png" class="img-packshot">
                    <div class="modal-body">
                        <h3>ขอบคุณค่ะ</h3>
                        <p>ทางทีม เพอร์ร่า ได้รับข้อมูลของคุณเรียบร้อยแล้ว<br> และทางทีมจะติดต่อคุณกลับไปโดยเร็วค่ะ</p>
                        <img src="assets/images/contact/back_to_home.png" class="center-block" data-dismiss="modal">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /#contact-thank -->


</div>

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script>
	function init_map() {
		var var_location = new google.maps.LatLng(<?php echo $lat;?>, <?php echo $lng;?>);
		var var_mapoptions = {
			center: var_location,
			zoom: 17
	};
	var var_marker = new google.maps.Marker({
		position: var_location,
		map: var_map,
		title:"<?php echo $contact ? $contact->company_name : "";?>"});
	var var_map = new google.maps.Map(document.getElementById("map-container"),
		var_mapoptions);
		var_marker.setMap(var_map);	
	}
	google.maps.event.addDomListener(window, 'load', init_map);
</script>