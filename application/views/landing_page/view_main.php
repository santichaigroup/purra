<html>
<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<title>สิงห์ คอร์เปอเรชั่น | Singha Corporation</title>
	<link rel="canonical" href="http://www.singha.com" />
	<base href="<?php echo base_url('public');?>/" />
	<style type="text/css">
	html, body{
		margin: 0;
		padding: 0;
		background-image: url("image/king/king.jpg");
		background-color: #000000;
		background-position: center;
		background-size: cover;
		background-repeat: no-repeat;
		width: 100%;
		height: 100%;
		min-height: 500px;
		position: relative;
	}

	.gotohome_btn{
		position: absolute;
		left: 0;
		right: 0;
		border: 1px solid white;
		color: white;
		text-decoration: none;
		text-align: center;
    	margin: 0 auto;
    	bottom: 20%;
    	font-size: 18px;
    	width: 180px;
    	height: 40px;
    	line-height: 40px;
	}
	@media (max-width: 768px) {
		html, body{
			margin: 0;
			padding: 0;
			background-image: url("image/king/king_m.jpg");
			background-color: #000000;
			background-position: top;
			background-size: contain;
			background-repeat: no-repeat;
			width: 100%;
			height: 100%;
			min-height: 480px;
			position: relative;

		}
		.gotohome_btn{
	    	top: 40%;
	    	font-size: 14px;
	    	width: 120px;
	    	height: 30px;
	    	line-height: 30px;
		}
	}
	@media (max-width: 768px) and (orientation: landscape) {
		.gotohome_btn{
	    	top: 65%;
	    	font-size: 14px;
	    	width: 120px;
	    	height: 30px;
	    	line-height: 30px;
		}
	}


	@media (max-width: 480px) {
		.gotohome_btn{
	    	top: 35%;
	    	font-size: 10px;
	    	width: 95px;
	    	height: 25px;
	    	line-height: 25px;
		}
	}
	</style>
</head>
<body>
    <a href="<?php echo site_url('home');?>" class="gotohome_btn">เข้าสู่เว็บไซต์</a>
</body>
</html>