<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>สิงห์ คอร์เปอเรชั่น | Singha Corporation</title>
    <meta name="description" content="บริษัท สิงห์ คอร์เปอเรชั่น จำกัด บริหารการดำเนินงานของบริษัทในเครือบุญรอด ดูแลธุรกิจ เช่น เบียร์, โซดา, เครื่องดื่ม">
	<meta name="keyword" content="สิงห์, สิงห์ คอร์เปอเรชั่น, อาหาร, เครื่องดื่ม, เบียร์ , singha , singha beer" />
        <base href="<?php echo base_url('public');?>/image/king-landing2/" />
	<link href="assets/css/style.css" type="text/css" rel="stylesheet" /> 
    <link rel="canonical" href="http://www.singha.com" />
	<!--GA CODE-->
    <script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-72905752-1', 'auto');
		ga('create', 'UA-78555141-1', 'auto', 'AllSite');
		ga('send', 'pageview');
		ga('AllSite.send', 'pageview');
	
	</script>
</head>
<body>
	<div id="wrapper">
		<div class="king_cover"><img src="assets/images/king1x.png" alt="Singha_สิงห์ คอร์เปอเรชั่น"></div>
		<div class="heading start">
			พระทรงเป็นพลังของแผ่นดิน<br>
			ทรงสถิตคู่ไทย ตราบนิรันดร์
		</div>
		<div class="line">
			<img src="assets/images/line1x.png" class="x1">
			<img src="assets/images/line2x.png" class="x2">
		</div>
		<div class="text">
			ขอน้อมเกล้าน้อมกระหม่อมรำลึก<span class="br_xs"></span><span class="br_default"></span>ในพระมหากรุณาธิคุณหาที่สุดมิได้<br>
			ข้าพระพุทธเจ้า คณะผู้บริหารและพนักงาน<br>
			สิงห์ คอร์เปอเรชั่น และบริษัทในเครือ
		</div>

		<div class="heading end en">
			With deep sorrow and devotion,<br>
			we grieve the loss of our Great King and pay our<br>most humble respects to His Majesty.
		</div>
		<div class="text en">
			Forever your grateful servants, <br>the management and staff of <br>Singha Corporation and affiliated companies.
		</div>

		<div class="btn">
                    <a href="<?php echo _site_url("home");?>">
				เข้าสู่เว็บไซต์หลัก<br>
				<span>Enter Site</span>
			</a> 
			
		</div>
	</div>
</body>
</html>