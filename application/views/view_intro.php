<!DOCTYPE html>
<html lang="en"><head>
    <?php
        $seo            = getPurra()->seo();
        $seo            = @json_decode($seo);
        $title          = @$seo->data->seo[0]->title ? $seo->data->seo[0]->title : "PURRA : feel so good : น้ำแร่ธรรมชาติ 100% ตราเพอร์ร่า";
        $keyword        = @$seo->data->seo[0]->keyword ? $seo->data->seo[0]->keyword : "";
        $googleAnalytic = @$seo->data->seo[0]->analytics ? $seo->data->seo[0]->analytics : "";
        $googleAnalytic = str_replace("\\n", "", str_replace("\\r\\n", "", $googleAnalytic));
        if( @$_TITLE ){
            $title = $_TITLE;
        }
        if( @$_KEYWORD ){
            $keyword = $_KEYWORD;
        }
        $description    = @$_DESC ? str_replace("&nbsp;", "", $_DESC) : "น้ำแร่ธรรมชาติ 100% ตราเพอร์ร่า";
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <meta property="og:url"         content="<?php echo _site_url("")?>" /> 
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Purra Feel so good: 100% natural mineral water" />
    <meta property="og:description"   content="น้ำแร่ธรรมชาติแหล่งพระงาม ดื่มให้กับทุกความรู้สึก น้ำแร่ธรรมชาติ 100% ตราเพอร์ร่า Feel so good " /> 
    <meta property="og:image"         content="<?php echo base_url('public/image/thumb-share-1200x630.jpg');?>" />

    <title><?php echo $title;?></title>
    <meta name="keywords" content="<?php echo $keyword;?>" />
    <meta name="description" content="<?php echo $description;?>">
    
    <base href="<?php echo base_url('public');?>/" />
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="assets/css/intro.css">

    <body class="intro">
        <?php
            $content = getPurra()->youtube();
            $content = @json_decode($content);
            
            $youtubeUrl= @$content->data->url ? trim($content->data->url) : "";
            $youtubeId = "";
            
            if($youtubeUrl){
                if(strstr( $youtubeUrl , "/embed/")){
                    $temp   = explode("/embed/", $youtubeUrl);
                    if(is_array($temp)  &&  count($temp)>1 ){
                       $youtubeId = $temp[1]; 
                    }
                }
                else if( strstr( $youtubeUrl , "?v=") ){
                    $temp   = explode("?v=", $youtubeUrl);
                    if(is_array($temp)  &&  count($temp)>1 ){
                       $youtubeId = $temp[1]; 
                    }
                }
            }
            
        ?>
	<div class="logo-purra"><img src="assets/images/logo_purra.png"></div>
	<a class="enter" href="<?php echo _site_url("home");?>"></a>
	<div id="background-video" class="background-video player" 
             data-property="{videoURL:'http://youtu.be/<?php echo $youtubeId;?>',
				containment:'self',
				autoplay: true,
                                optimizeDisplay: true,
                                showControls: false,
                                startAt: 0,
                                opacity: 1,	
                                loop:false,
                                addRaster: false}"></div>

	<!-- javascript -->
	<script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="js/jquery.mb.YTPlayer-3.0.6/dist/jquery.mb.YTPlayer.min.js"></script>
	<script>
	$(function(){
            $("#background-video").YTPlayer({
                autoplay: 1,
	        autoPlay: true,
                showControls : false
            });
            $('#background-video').on("YTPEnd",function(e){
                window.location="<?php echo _site_url("home");?>";
            });
	});
	</script>
	
        <?php echo $googleAnalytic;?>
        
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-78555141-1', 'auto');
            ga('send', 'pageview');
        </script>

    </body>
</html>