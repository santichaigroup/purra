<div class="mainContent">


    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->
    

    <div class="banner">
    	<div class="scrollto"><a href="#social" class="scroll"></a></div>
    </div><!-- /.banner -->
    

    <?php $this->load->view("home/social");?>
    <!-- /.social -->
    

    <div class="news-activities">
        <div class="container">

            <div class="row">
                <?php $this->load->view("home/lifestyle");?>
            	<?php $this->load->view("home/promotion_activity");?>
            </div>
		        
        </div><!-- /.container -->
    </div><!-- /.News & Activities -->
    

    <?php $this->load->view("home/product");?>
    <!-- /.product-purra -->
 
    
    
    <script>
        $(function(){
            $(window).resize(function(){
                var div_fb_pluginFanpage = $(".box-shadow.feed-facebook");
                loadFacebookFanpagePlugin(
                        div_fb_pluginFanpage, 
                        div_fb_pluginFanpage.width(), 
                        div_fb_pluginFanpage.height()
                );
            });
            
            var div_fb_pluginFanpage = $(".box-shadow.feed-facebook");
            loadFacebookFanpagePlugin(
                    div_fb_pluginFanpage, 
                    div_fb_pluginFanpage.width(), 
                    div_fb_pluginFanpage.height()
            );
        });
    </script>
    
</div>