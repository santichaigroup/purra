<?php
    $instagram      = getPurra()->instagram();
    $instagram      = json_decode($instagram);
    $hashtag        = @$instagram->data->source->hashtag->hashtags ? 
                        $instagram->data->source->hashtag->hashtags 
                        : "";
?>
<div class="social" id="social">
    <div class="container">

            <div class="row">
            <div class="col-md-8 col-sm-7">

                <div data-sr="over 2s">
                <div class="heading-instagram">
                    <div class="hashtag">
                        <!--<span>#purra</span><span>#naturalmineral</span><span>#water</span><span>#healthy</span><span>#mineralwater</span>-->
                        <span><?php echo $hashtag;?></span>
                    </div>
                </div>
                <div class="social-gallery">
                    <div id="owl-social" class="owl-carousel owl-theme">
                        <?php
                            $_source = null;
                            if( @$instagram->data->source->{'custom-upload'}->images ){
                                $_source = &$instagram->data->source->{'custom-upload'};
                            }else if( @$instagram->data->source->hashtag->images ){
                                $_source = &$instagram->data->source->hashtag;
                            }
                            
                            if( $_source!=null  &&  $_source->images ){
                                $len        = count($_source->images);
                                for( $i=0;  $i<$len; ){
                                    echo '<div>';
                                    
                                    if( @$_source->images[$i] ){
                                        $item      = $_source->images[$i];
                                        ?>
                                            <div class="item"><img src="<?php echo @$item->photo_standard?>"></div>
                                        <?php
                                        $i++;
                                    }
                                    
                                    
                                    if( @$_source->images[$i] ){
                                        $item      = $_source->images[$i];
                                        ?>
                                            <div class="item"><img src="<?php echo @$item->photo_standard?>"></div>
                                        <?php
                                        $i++;
                                    }
                                    
                                    
                                    if( @$_source->images[$i] ){
                                        $item      = $_source->images[$i];
                                        ?>
                                            <div class="item"><img src="<?php echo @$item->photo_standard?>"></div>
                                        <?php
                                        $i++;
                                    }
                                    
                                    echo '</div>';
                                }
                                
                            }
                        ?>
                        
                    </div>
                </div><!-- /.instagram -->
                </div><!-- /data-sr -->

            </div>
            <div class="col-md-4 col-sm-5">

                    <div data-sr="move -15px, over 2s">
                    <div class="heading-youtube"></div>
                    <div class="box-shadow video">
                        <div class="embed-responsive embed-responsive-16by9">
                            <?php
                                /*
                                    $content = getPurra()->youtube();
                                    $content = @json_decode($content);

                                    $youtubeUrl= @$content->data->url ? trim($content->data->url) : "";
                                    $youtubeId = "";

                                    if($youtubeUrl){
                                        if(strstr( $youtubeUrl , "/embed/")){
                                            $temp   = explode("/embed/", $youtubeUrl);
                                            if(is_array($temp)  &&  count($temp)>1 ){
                                               $youtubeId = $temp[1]; 
                                            }
                                        }
                                        else if( strstr( $youtubeUrl , "?v=") ){
                                            $temp   = explode("?v=", $youtubeUrl);
                                            if(is_array($temp)  &&  count($temp)>1 ){
                                               $youtubeId = $temp[1]; 
                                            }
                                        }
                                    }
                                 <iframe width="100%" height="" src="https://www.youtube.com/embed/<?php echo $youtubeId;?>?rel=0" frameborder="0" allowfullscreen></iframe>
                                 * 
                                 */
                            
                            
                                $content = getPurra()->youtube();
                                $content = @json_decode($content);
                                if( $content
                                        && @$content->data->medias
                                        &&  count($content->data->medias)>0)
                                {
                                    $index  = rand(0, count($content->data->medias)-1);
                                    $media  = $content->data->medias[$index];
                                ?>
                                    <iframe width="100%" height="" src="<?php echo $media->url;?>?rel=0" frameborder="0" allowfullscreen></iframe>
                                <?php
                                }
                            ?>
                        </div>
                    </div><!-- /.youtube -->

                    <div class="heading-facebook"></div>
                    <div class="box-shadow feed-facebook">
                        <?php
                            $this->load->view("include/facebook_fanpage_plugin", array(
                                'height' => 280
                            ));
                        ?>	
                    </div><!-- /.facebook -->
                </div><!-- /data-sr -->

            </div>
        </div>

    </div><!-- /.container -->
</div>