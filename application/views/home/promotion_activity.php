<div class="col-md-6 col-sm-6 col-xs-12">
                	
    <div data-sr>
        <div class="heading">
            <img src="assets/images/home/promotions_activities.png">
            <div class="view-all">
                <a href="<?php echo _site_url("promotion");?>">view all <span class="glyphicon glyphicon-plus-sign"></span></a>
            </div>
        </div>

        <?php
            $rs     = getPurra()->promotionActivity(10);
            $rs     = @json_decode($rs);
            if( @$rs->code!='200' ){
                $rs = null;
            }
        ?>
        <ul id="owl-promotion" class="list-items owl-carousel owl-theme hidden-xs">
            <?php
                // Desktop
                if( $rs ){
                    $len    = is_array($rs->data->content) ? count($rs->data->content) : 0;
                    for( $i=0;  $i<3;  $i++ ){
                        if( @$rs->data->content[$i] ){
                            $item   = @$rs->data->content[$i];
                            $url = _site_url("promotion/". ($item->type=="promotion"?"promotionDetail":"activityDetail"). "/{$item->id}");
                            $item->thumb_desktop = $item->thumb_desktop ? $item->thumb_desktop : base_url("public/image/thumb-no-img.jpg");
                            $image  = $item->thumb_desktop;
                        ?>
                            <li>
                                <div class="thumbnail">
                                    <a href="<?php echo $url;?>">
                                        <img src="<?php echo $image;?>" alt="<?php echo @$item->alt;?>" />
                                    </a>
                                    <p><?php echo $item->name;?></p>
                                    <a href="<?php echo $url;?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                                </div>
                            </li>
                        <?php
                        }
                    }
                }
            ?>
        </ul>

        
        <ul id="owl-promotion" class="list-items owl-carousel owl-theme hidden-lg hidden-md hidden-sm">
            <?php
                // Mobile
                if( $rs ){
                    $len    = is_array($rs->data->content) ? count($rs->data->content) : 0;
                    for( $i=0;  $i<$len;  $i++ ){
                        if( @$rs->data->content[$i] ){
                            $item   = $rs->data->content[$i];
                            $url = _site_url("promotion/". ($item->type=="promotion"?"promotionDetail":"activityDetail"). "/{$item->id}");
                            $item->thumb_mobile = $item->thumb_mobile ? $item->thumb_mobile : base_url("public/image/thumb-no-img.jpg");
                            $image  = $item->thumb_mobile;
                            if( $mobileDetect->isTablet() ){
                                $image = $item->thumb_tablet;
                            }
                        ?>
                            <li>
                                <div class="thumbnail">
                                    <a href="<?php echo $url;?>">
                                        <img src="<?php echo $image;?>" alt="<?php echo @$item->alt;?>" />
                                    </a>
                                    <p><?php echo $item->name;?></p>
                                    <a href="<?php echo $url;?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                                </div>
                            </li>
                        <?php
                        }
                    }
                }
            ?>
        </ul>
        

        <div class="view-all">
            <a href="<?php echo _site_url("promotion");?>">view all <span class="glyphicon glyphicon-plus-sign"></span></a>
        </div>
    </div>

</div>