<div class="col-md-6 col-sm-6 col-xs-12">
                
    <div data-sr>
        <div class="heading">
            <img src="assets/images/home/lifestyle.png">
            <div class="view-all">
                <a href="<?php echo _site_url("lifestyle/showlist");?>">view all <span class="glyphicon glyphicon-plus-sign"></span></a>
            </div>
        </div>

        
        <?php
            $rs     = getPurra()->lifestyle(0, 10);
            $rs     = @json_decode($rs);
            if( @$rs->code!='200' ){
                $rs = null;
            }
        ?>
        <ul id="owl-lifestyle" class="list-items owl-carousel owl-theme hidden-xs">
            <?php
                // Desktop
                if( $rs ){
                    $total  = $rs->data->quantity;
                    $len    = is_array($rs->data->lifestyle) ? count($rs->data->lifestyle) : 0;
                    for( $i=0;  $i<3;  $i++ ){
                        if( @$rs->data->lifestyle[$i] ){
                            $item   = $rs->data->lifestyle[$i];
                            $item->thumb_desktop = $item->thumb_desktop ? $item->thumb_desktop : base_url("public/image/thumb-no-img.jpg");
                        ?>
                            <li>
                                <div class="thumbnail">
                                    <a href="<?php echo _site_url("lifestyle/detail/{$item->id}");?>">
                                        <img src="<?php echo $item->thumb_desktop;?>" alt="<?php echo @$item->al;?>" />
                                    </a>
                                    <p><?php echo $item->name;?></p>
                                    <a href="<?php echo _site_url("lifestyle/detail/{$item->id}");?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                                </div>
                            </li>
                        <?php
                        }
                    }
                }
            ?>
        </ul>
        
        
        
        <ul id="owl-lifestyle" class="list-items owl-carousel owl-theme hidden-lg hidden-md hidden-sm">
            <?php
                // Mobile
                if( $rs ){
                    $total  = $rs->data->quantity;
                    $len    = count($rs->data->lifestyle);
                    for( $i=0;  $i<$len;  $i++ ){
                        $item   = $rs->data->lifestyle[$i];
                        $item->thumb_mobile = $item->thumb_mobile ? $item->thumb_mobile : base_url("public/image/thumb-no-img.jpg");
                        
                        $image = $item->thumb_mobile;
                        if( $mobileDetect->isTablet() ){
                            $image = $item->thumb_tablet;  
                        }
                    ?>
                        <li>
                            <div class="thumbnail">
                                <a href="<?php echo _site_url("lifestyle/detail/{$item->id}");?>">
                                    <img src="<?php echo $image;?>" alt="<?php echo @$item->alt;?>" />
                                </a>
                                <p><?php echo $item->name;?></p>
                                <a href="<?php echo _site_url("lifestyle/detail/{$item->id}");?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                            </div>
                        </li>
                    <?php
                    }
                }
            ?>
        </ul>
        
        

        <div class="view-all">
            <a href="<?php echo _site_url("lifestyle/showlist");?>">view all <span class="glyphicon glyphicon-plus-sign"></span></a>
        </div>
    </div>

</div>