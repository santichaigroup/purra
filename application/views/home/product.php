<?php
    $productHome        = getPurra()->product_home();
    $productHome        = @json_decode($productHome);
    if( $productHome  &&  @$productHome->data->product[0]){
        $data           = $productHome->data->product[0];
        $backgroundProduct = $data->photo_desktop;
        if( $mobileDetect->isMobile() ){
            $backgroundProduct = $data->photo_mobile;
        } 
        else if( $mobileDetect->isTablet() ){
            $backgroundProduct = $data->photo_tablet;
        }
    ?>
        <div class="product-purra"
             style="background: #e1f0f7 url(<?php echo $backgroundProduct;?>) no-repeat bottom center;">
            <div class="container">

                <div class="row">
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="detail" data-sr>
                            <?php 
                                $detail     = str_get_html($data->detail);
                                $imgs       = $detail->find("img");
                                if( count($imgs)>0 ){
                                    $img_len    = count($imgs);
                                    for( $i=0;  $i<$img_len;  $i++ ){
                                        $img    = $imgs[$i];
                                        if( strstr($img->attr['src'], '../')  
                                            && substr($img->attr['src'], 0, 4)!='http' )
                                        {
                                           $img->attr['src'] = getPurra()->getPrefixUrl() . str_replace('../', '', $img->attr['src']); 
                                        }
                                    }
                                }
                                echo $detail->save();
                            ?>
                        </div>
                    </div>
                </div>

                <div class="move_up"></div>
            </div><!-- /.container -->
        </div>
    <?php
    }else{
    ?>
        <div class="product-purra">
            <div class="container">

                <div class="row">
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="detail" data-sr>
                        <p><i>เพอร์ร่า</i> น้ำแร่ธรรมชาติ 100% จากแหล่งน้ำใต้ดินตาม<br class="desktop">ธรรมชาติ<br class="mobile"> ลึกเกือบ 1000 ฟุต ซึ่งสะสม<br>แร่ธาตุมาอย่างยาวนาน<br class="desktop">เพราะครั้งหนึ่ง<br class="mobile">เมื่อ 18,000 ปีที่แล้ว เคยเป็นทะเลอันอุดสมบูรณ์<br class="desktop"><br class="mobile"> ทำให้น้ำแร่เพอร์ร่าเป็นน้ำแร่ที่เปี่ยมไปด้วยแร่ธาตุที่เป็นประโยชน์<br class="desktop">ต่อร่างกาย<br class="mobile">ครบในขวดเดียว <span>ไม่มีการ<br>เติมแต่งแร่ธาตุใดๆ ดื่มง่าย<br class="desktop">รสชาติดี</span> และจัดเป็นน้ำแร่<br class="mobile">ชนิดเบา ทำให้สามารถดื่มได้ทุกวัน <span class="img"><img src="assets/images/home/feel_so_good.png"></span></p>

                        </div>
                    </div>
                </div>

                <div class="move_up"></div>
            </div><!-- /.container -->
        </div>
    <?php
    }
?>

<script>
        $(function(){
                $(".move_up").click(function() {
                        $('html, body').animate({
                                scrollTop: $("html").offset().top
                        }, 400);
                });	
        });
</script>