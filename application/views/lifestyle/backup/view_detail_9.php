
<div class="mainContent">


    <div class="share">
        <ul class="share-social">
        	<li><a href="https://www.facebook.com/feelsogood.purra" target="_blank"></a></li>
        	<li><a href="https://instagram.com/purra_th" target="_blank"></a></li>
        	<li><a href="https://www.youtube.com/channel/UCmQhlDw76twDawJ_oj44eCQ" target="_blank"></a></li>
        </ul>
    </div>
    <!-- /#share -->
    

    <div class="container">
    
    	<div class="row">
        	<div class="col-md-8 col-xs-12">
            	
                <div data-sr>
                <ol class="breadcrumb">
                    <li><a href="<?php echo _site_url("home");?>">Home</a></li>
                    <li><a href="<?php echo _site_url("lifestyle");?>">Lifestyle</a></li>
                    <li><a>ดื่มให้กับความรู้สึกกล้า สิ่งใหม่ๆ รอให้ได้สัมผัสอยู่เพียบ</a></li>
                </ol>
                
                <div class="box-shadow content">
                
                    <h2>ดื่มให้กับความรู้สึกกล้า สิ่งใหม่ๆ รอให้ได้สัมผัสอยู่เพียบ</h2>
                    
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
                    
                        <div class="carousel-inner">
                            <div class="item active"><img src="assets/images/lifestyle/thumb/9.jpg" alt=""></div>
                        </div><!-- carousel-inner -->
                        
                    </div><!--/.carousel -->
                    
                    <div class="detail">
                        <p>เกิดมาทั้งทีเมื่อมีโอกาสให้ไขว่คว้า จงกล้าที่หยิบและใช้มันให้เต็มที่ เชื่อสิ !สิ่งใหม่ๆ รอให้ได้สัมผัสอยู่เพียบ ... และเริ่มต้นสู่เส้นชัยทุกรูปแบบด้วยการ "กล้า" ที่จะดื่มน้ำแร่เพอร์ร่า เพื่อเปิดรับสิ่งดีๆ ให้ชีวิตกันนะคะ
<span>#PurraFeelSoGood</span></p>
                    </div>
                    
                    <?php
                        $this->load->view("include/social_share_button", array(
                            "url" => current_url(),
                            "title" => $_TITLE,
                            "caption" => $_DESC,
                            "image" => $_IMAGE
                        ));
                    ?>

                    <nav>
                        <ul class="pager">
                            <li class="previous"><a href="<?php echo _site_url("lifestyle/detail/8");?>"><span class="glyphicon glyphicon-triangle-left"></span> ย้อนกลับ</a></li>
                            <li class="next"><a href="<?php echo _site_url("lifestyle/detail/10");?>">ถัดไป <span class="glyphicon glyphicon-triangle-right"></span></a></li>
                        </ul>
                    </nav>                    
                    
                </div><!-- /.content -->
                </div><!-- /data-sr -->
                
            </div>
        	<div class="col-md-4 col-xs-12">
            
            	<div data-sr>
                <div class="sidebar">
                	
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#tab_facebook" data-toggle="pill"><i class="facebook"></i></a></li>
                        <li><a href="#tab_instagram" data-toggle="pill"><i class="instagram"></i></a></li>
                        <li><a href="#tab_youtube" data-toggle="pill"><i class="youtube"></i></a></li>
                    </ul>
                    
                    <div class="box-shadow">

                        <?php
                            $this->load->view("include/view_detail_social_tab");
                        ?>
                        <!--/.tab-content -->
                        
                        <div class="related-content">
                        	<img src="assets/images/article/all_article.png" alt="บทความอื่นๆ">
                            <ul>
                            	<li>
                                    <a href="<?php echo _site_url("lifestyle/detail/1");?>"><img src="assets/images/lifestyle/thumb/1.jpg"></a>
                                    <p>เลือกกินคาร์โบไฮเดรตชนิดดี ฟิตเต็มที่พร้อมวิ่งได้ไกล</p>
                                </li>
                                <li>
                                    <a href="<?php echo _site_url("lifestyle/detail/2");?>"><img src="assets/images/lifestyle/thumb/2.jpg"></a>
                                    <p>ดูแลสายตาด้วย 5 สเตปง่ายๆ </p>
                                </li>
                                <li>
                                    <a href="<?php echo _site_url("lifestyle/detail/3");?>"><img src="assets/images/lifestyle/thumb/3.jpg"></a>
                                    <p>ทำสมาธิ 10 นาที มีดีเรื่องอายุ</p>
                                </li>
                            </ul>
                            <div class="view-all">
                            	<a href="<?php echo _site_url("lifestyle/showlist");?>">view all <span class="glyphicon glyphicon-plus-sign"></span></a>
                            </div>
                        </div><!--/.related-content -->
                    
                    </div>
                    
                </div><!--/.sidebar -->
                </div><!-- /data-sr -->
                
            </div>
        </div>
        
    
        <div class="move_up"></div>
    </div><!-- /.container -->
    

</div><!-- /.mainContent -->
