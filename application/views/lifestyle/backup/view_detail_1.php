<div class="mainContent">


    <div class="share">
        <ul class="share-social">
        	<li><a href="https://www.facebook.com/feelsogood.purra" target="_blank"></a></li>
        	<li><a href="https://instagram.com/purra_th" target="_blank"></a></li>
        	<li><a href="https://www.youtube.com/channel/UCmQhlDw76twDawJ_oj44eCQ" target="_blank"></a></li>
        </ul>
    </div>
    <!-- /#share -->
    

    <div class="container">
    
    	<div class="row">
        	<div class="col-md-8 col-xs-12">
            	
                <div data-sr>
                <ol class="breadcrumb">
                    <li><a href="<?php echo _site_url("home");?>">Home</a></li>
                    <li><a href="<?php echo _site_url("lifestyle");?>">Lifestyle</a></li>
                    <li><a>เลือกกินคาร์โบไฮเดรตชนิดดี ฟิตเต็มที่พร้อมวิ่งได้ไกล</a></li>
                </ol>
                
                <div class="box-shadow content">
                
                    <h2>เลือกกินคาร์โบไฮเดรตชนิดดี ฟิตเต็มที่พร้อมวิ่งได้ไกล</h2>
                    
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
                    
                        <div class="carousel-inner">
                            <div class="item active"><img src="assets/images/lifestyle/thumb/1.jpg" alt=""></div>
                        </div><!-- carousel-inner -->
                        
                    </div><!--/.carousel -->
                    
                    <div class="detail">
                        <p>สาวๆ หลายคนกลัวการกินคาร์โบไฮเดรตกันใช่มั้ยล่ะ เพราะกลัวว่ากินแล้วอ้วน วิ่งเท่าไหร่ก็เอาออกไม่หมด แต่ถ้าเราเลือกกินให้เป็น ก็เบิร์นออกง่ายนะ แนะนำให้รู้จักคาร์โบไฮเดรตชนิดดีพวก ถั่ว ธัญพืช โฮลวีท โฮลเกรน ที่มีไฟเบอร์สูง น้ำตาลต่ำ สารอาหารเพียบ ประสิทธิภาพการเผาผลาญก็สูง ได้พลังงานพร้อมฟิตไปวิ่งแบบไม่ต้องมากังวลเรื่องอ้วนด้วย ! <span>#FoodTips</span><span>#PurraFeelSoGood</span></p>
                    </div>
                    
                    
                    
                    
                    <?php
                        $this->load->view("include/social_share_button", array(
                            "url" => current_url(),
                            "title" => $_TITLE,
                            "caption" => $_DESC,
                            "image" => $_IMAGE
                        ));
                    ?>
                        
                        

                    <nav>
                        <ul class="pager">
                            <li class="previous disabled"><a href="javascript:void(0);"><span class="glyphicon glyphicon-triangle-left"></span> ย้อนกลับ</a></li>
                            <li class="next"><a href="<?php echo _site_url("lifestyle/detail/2");?>">ถัดไป <span class="glyphicon glyphicon-triangle-right"></span></a></li>
                        </ul>
                    </nav>                    
                    
                </div><!-- /.content -->
                </div><!-- /data-sr -->
                
            </div>
        	<div class="col-md-4 col-xs-12">
            
            	<div data-sr>
                <div class="sidebar">
                	
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#tab_facebook" data-toggle="pill"><i class="facebook"></i></a></li>
                        <li><a href="#tab_instagram" data-toggle="pill"><i class="instagram"></i></a></li>
                        <li><a href="#tab_youtube" data-toggle="pill"><i class="youtube"></i></a></li>
                    </ul>
                    
                    <div class="box-shadow">

                        <?php
                            $this->load->view("include/view_detail_social_tab");
                        ?>
                        <!--/.tab-content -->
                        
                        <div class="related-content">
                            <img src="assets/images/article/all_article.png" alt="บทความอื่นๆ">
                            <ul>
                            	<li>
                                    <a href="<?php echo _site_url("lifestyle/detail/1");?>"><img src="assets/images/lifestyle/thumb/1.jpg"></a>
                                    <p>เลือกกินคาร์โบไฮเดรตชนิดดี ฟิตเต็มที่พร้อมวิ่งได้ไกล</p>
                                </li>
                                <li>
                                    <a href="<?php echo _site_url("lifestyle/detail/2");?>"><img src="assets/images/lifestyle/thumb/2.jpg"></a>
                                    <p>ดูแลสายตาด้วย 5 สเตปง่ายๆ </p>
                                </li>
                                <li>
                                    <a href="<?php echo _site_url("lifestyle/detail/3");?>"><img src="assets/images/lifestyle/thumb/3.jpg"></a>
                                    <p>ทำสมาธิ 10 นาที มีดีเรื่องอายุ</p>
                                </li>
                            </ul>
                            <div class="view-all">
                            	<a href="<?php echo _site_url("lifestyle/showlist");?>">view all <span class="glyphicon glyphicon-plus-sign"></span></a>
                            </div>
                        </div><!--/.related-content -->
                    
                    </div>
                    
                </div><!--/.sidebar -->
                </div><!-- /data-sr -->
                
            </div>
        </div>
        
    
        <div class="move_up"></div>
    </div><!-- /.container -->
    

</div>
