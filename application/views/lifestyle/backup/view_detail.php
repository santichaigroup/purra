<div class="mainContent">


    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->
    

    <div class="container">
    
    	<div class="row">
        	<div class="col-md-8 col-xs-12">
            	
                <div data-sr>
                <ol class="breadcrumb">
                    <li><a href="<?php echo _site_url("home");?>">Home</a></li>
                    <li><a href="<?php echo _site_url("lifestyle");?>">Lifestyle</a></li>
                    <li><a>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</a></li>
                </ol>
                
                <div class="box-shadow content">
                
                    <h2>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</h2>
                    
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
                    
                        <div class="carousel-inner">
                            <div class="item active"><img src="assets/images/article/thumb/1.jpg" alt="1.jpg"></div>
                            <div class="item"><img src="assets/images/article/thumb/1.jpg" alt="2.jpg"></div>
                            <div class="item"><img src="assets/images/article/thumb/1.jpg" alt="3.jpg"></div>
                            <div class="item"><img src="assets/images/article/thumb/1.jpg" alt="4.jpg"></div>
                            <div class="item"><img src="assets/images/article/thumb/1.jpg" alt="5.jpg"></div>
                            <div class="item"><img src="assets/images/article/thumb/1.jpg" alt="6.jpg"></div>
                            <div class="item"><img src="assets/images/article/thumb/1.jpg" alt="7.jpg"></div>
                        </div><!-- carousel-inner -->
                        
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a><!-- Controls -->
                        
                        <ul class="thumbnails-carousel clearfix">
                            <li><img src="assets/images/article/thumb/1.jpg" alt="1.jpg"></li>
                            <li><img src="assets/images/article/thumb/2.jpg" alt="2.jpg"></li>
                            <li><img src="assets/images/article/thumb/3.jpg" alt="3.jpg"></li>
                            <li><img src="assets/images/article/thumb/4.jpg" alt="4.jpg"></li>
                            <li><img src="assets/images/article/thumb/5.jpg" alt="5.jpg"></li>
                            <li><img src="assets/images/article/thumb/6.jpg" alt="6.jpg"></li>
                            <li><img src="assets/images/article/thumb/7.jpg" alt="7.jpg"></li>
                        </ul><!-- Thumbnails -->
                        
                    </div><!--/.carousel -->
                    
                    <div class="detail">
                        <p>หลังจากประสบความสำเร็จอย่างถล่มทลายในปีที่แล้ว มหกรรมการวิ่งบนเส้นทางแห่งเสียงดนตรีสไตล์อินเตอร์แอคทีฟ “เดอะมิวสิค รัน บาย เอไอเอ (The Music Run by AIA)” กำลังจะกลับมาระเบิดความมันส์อีกครั้ง เพื่อมอบความสนุกสนานที่ยิ่งใหญ่กว่าเดิม! ใน วันเสาร์ที่ 7 พฤศจิกายน 2558 ณ สวนหลวง ร.9 เพื่อให้ผู้รักสุขภาพที่มีดนตรีและกีฬาในหัวใจได้มาร่วมวิ่งไปบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพที่หลากหลาย ซึ่งกิจกรรม เดอะมิวสิค รัน ตระเวนจัดมาแล้วทั่วทวีปเอเชีย ทั้งในสิงคโปร์ ไต้หวัน อินโดนีเซีย มาเลเซีย และเมียนมาร์ ฯลฯ โดยมีผู้เข้าร่วมกิจกรรมมากกว่า 50,000 คน และคาดว่าครั้งนี้จะได้รับความสนใจจากบรรดานักวิ่งแนวไลฟ์สไตล์ในเมืองไทยไม่แพ้กัน </p>
                        <p>เดอะมิวสิค รัน บาย เอไอเอ 2015 นำเสนอประสบการณ์ทางดนตรีและการออกกำลังกายในสไตส์ใหม่ที่ให้ผู้เข้าร่วมกิจกรรมสามารถ เดิน วิ่ง เต้น ร้อง และสนุกสนานไปกับเสียงเพลงตลอด 5 กิโลเมตร ที่จัดขึ้นไว้แตกต่างกันถึง 5 โซน ทั้งแนวร็อค พ็อพ เรโทร ฮิพฮอพ และแดนซ์</p>
                        <p>นอกจากกิจกรรมการวิ่งบนเส้นทางแห่งดนตรี ผู้ที่ลงทะเบียนเข้าร่วมงานยังสามารถร่วมสนุกเพื่อวอร์มอัพทางดนตรีกันก่อนวันงานจริง ในกิจกรรม “Your music, YourbeatTM” เลือกดนตรีที่ใช่ จังหวะที่ชอบด้วยตัวเอง โดยร่วมโหวตเพลงที่ชื่นชอบทางระบบ Music Genre บนเว็บไซต์ เลือกเพลงโปรดจากแนวเพลงทั้ง 5 ประเภท ซึ่งเพลงที่ได้รับการโหวตสูงสุด 5 เพลง ในแต่ละประเภทจะถูกนำไปเปิดใช้ในโซนดนตรีของแนวเพลงนั้นๆ ในวันงานจริง</p>
                        <p>หนึ่งในเอกลักษณ์ของงาน เดอะมิวสิค รัน ที่แตกต่างจากกิจกรรมการวิ่งงานอื่นๆ คือเป็นการวิ่งที่ไม่มีการจับเวลา ทำให้ทั้งนักกีฬาตัวจริงและนักวิ่งหน้าใหม่สามารถสนุกสนานไปกับการออกกำลังกายและเสียงเพลงในพื้นที่โซนต่างๆ ได้อย่างสบายใจตามความต้องการ ไร้ซึ่งความรู้สึกกดดันที่ต้องแข่งขันชิงชัยความเป็นหนึ่ง</p>
                        <p>เอไอเอ ประเทศไทย กลับมาในฐานะผู้สนับสนุนหลักอย่างเป็นทางการนำเสนอกิจกรรมเดอะมิวสิค รัน ที่กรุงเทพฯ อีกครั้งในปีนื้ ร่วมด้วยผู้สนับสนุนอย่างเป็นทางการอีกมากมาย ได้แก่ น้ำแร่ธรรมชาติเพอร์รา (Purra), จีเอส แบตเตอรี่ (GS Battery), อารี รันนิ่ง (Ari Running), โรงแรมสยาม แอท สยาม ดีไซน์ โฮเต็ล แอนด์ สปา (Siam at Siam Design Hotel and Spa), สายรัดข้อมือ ฟิตบิท (Fitbit) และ แกร็บคาร์ (GrabCar)</p>
                        <p>ผู้สนใจเข้าร่วมกิจกรรม สามารถซื้อบัตรลงทะเบียนที่ ไทยทิคเก็ตเมเจอร์ ทุกสาขา ได้ตั้งแต่ 24 สิงหาคม - 27 ตุลาคม 2558 กิจกรรมปีนี้ แบ่งการลงทะเบียนเป็น 2 ประเภท ได้แก่ ประเภทมาตรฐาน (STANDARD) ราคา 700 บาท ซึ่งผู้ลงทะเบียนจะได้รับชุดอุปกรณ์การวิ่ง ได้แก่ เสื้อกีฬา และถุงของที่ระลึกมากมาย ทั้งสติ๊กเกอร์เดอะ มิวสิค รัน, ริสแบนด์, สติ๊กเกอร์แทตทูลายเท่ และ ประเภทร็อค สตาร์ (ROCK STAR) ราคา 1,100 บาท สำหรับแฟนตัวจริง ให้คุณเก๋ไก๋กว่าใครด้วยกับอุปกรณ์แต่งกายสุดชิคเสริมพิเศษมากกว่าชุดมาตรฐาน</p>
                    </div>
                    
                    <?php
                        $this->load->view("include/social_share_button", array(
                            "url" => current_url(),
                            "title" => $_TITLE,
                            "caption" => $_DESC,
                            "image" => $_IMAGE
                        ));
                    ?>

                    <nav>
                        <ul class="pager">
                            <li class="previous"><a href="javascript:void(0);"><span class="glyphicon glyphicon-triangle-left"></span> ย้อนกลับ</a></li>
                            <li class="next"><a href="<?php echo _site_url("lifestyle/detail/1");?>">ถัดไป <span class="glyphicon glyphicon-triangle-right"></span></a></li>
                        </ul>
                    </nav>                    
                    
                </div><!-- /.content -->
                </div><!-- /data-sr -->
                
            </div>
        	<div class="col-md-4 col-xs-12">
            
            	<div data-sr>
                <div class="sidebar">
                	
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#tab_facebook" data-toggle="pill"><i class="facebook"></i></a></li>
                        <li><a href="#tab_instagram" data-toggle="pill"><i class="instagram"></i></a></li>
                        <li><a href="#tab_youtube" data-toggle="pill"><i class="youtube"></i></a></li>
                    </ul>
                    
                    <div class="box-shadow">

                        <?php
                            $this->load->view("include/view_detail_social_tab");
                        ?>
                        <!--/.tab-content -->
                        
                        <div class="related-content">
                        	<img src="assets/images/article/all_article.png" alt="บทความอื่นๆ">
                            <ul>
                            	<li>
                                    <a href="<?php echo _site_url("lifestyle/detail/1");?>"><img src="assets/images/lifestyle/thumb/1.jpg"></a>
                                    <p>เลือกกินคาร์โบไฮเดรตชนิดดี ฟิตเต็มที่พร้อมวิ่งได้ไกล</p>
                                </li>
                                <li>
                                    <a href="<?php echo _site_url("lifestyle/detail/2");?>"><img src="assets/images/lifestyle/thumb/2.jpg"></a>
                                    <p>ดูแลสายตาด้วย 5 สเตปง่ายๆ </p>
                                </li>
                                <li>
                                    <a href="<?php echo _site_url("lifestyle/detail/3");?>"><img src="assets/images/lifestyle/thumb/3.jpg"></a>
                                    <p>ทำสมาธิ 10 นาที มีดีเรื่องอายุ</p>
                                </li>
                            </ul>
                            <div class="view-all">
                            	<a href="<?php echo _site_url("lifestyle/showlist");?>">view all <span class="glyphicon glyphicon-plus-sign"></span></a>
                            </div>
                        </div><!--/.related-content -->
                    
                    </div>
                    
                </div><!--/.sidebar -->
                </div><!-- /data-sr -->
                
            </div>
        </div>
        
    
        <div class="move_up"></div>
    </div><!-- /.container -->
    

</div><!-- /.mainContent -->
