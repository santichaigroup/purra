<div class="mainContent">

    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->

    <div class="container">
    
    	<div class="row">
        	<div class="col-md-8 col-xs-12">
            	
                <div data-sr>
                <ol class="breadcrumb">
                    <li><a href="<?php echo _site_url("home");?>">Home</a></li>
                    <li><a href="<?php echo _site_url("lifestyle");?>">Lifestyle</a></li>
                    <li><a><?php echo utf8_substr(strip_tags($content->name), 0, 100);?></a></li>
                </ol>
                
                <div class="box-shadow content">
                
                    <h2><?php echo $content->name;?></h2>
                    
                    <?php
                        // Show gallery
                        if( @$content->gallery  && is_array($content->gallery)  &&  count($content->gallery)>0 ){
                        ?>
                            <div id="slider" class="flexslider" style="margin-bottom: 10px;">
                                <ul class="slides">
                                    <?php
                                        $len = count($content->gallery);
                                        for( $i=0;  $i<$len;  $i++ ){
                                            $gallery    = $content->gallery[$i];
                                            $img_alt    = @$gallery->alt;
                                        ?>
                                            <li>
                                                <img src="<?php echo $gallery->image;?>" alt="<?php echo $img_alt;?>" />
                                            </li>
                                        <?php 
                                        }
                                    ?>
                                </ul>
                            </div>

                            <div id="carousel" class="flexslider">
                                <ul class="slides">
                                    <?php
                                        $len = count($content->gallery);
                                        for( $i=0;  $i<$len;  $i++ ){
                                            $gallery    = $content->gallery[$i];
                                        ?>
                                            <li>
                                                <img src="<?php echo $gallery->image;?>" />
                                            </li>
                                        <?php 
                                        }
                                    ?>
                                </ul>
                            </div>


                            <script src="js/FlexSlider/jquery.flexslider-min.js"></script>
                            <link rel="stylesheet" type="text/css" href="js/FlexSlider/flexslider.css" />
                            <script>
                                $(window).load(function() {
                                    $('#carousel').flexslider({
                                        animation: "slide",
                                        controlNav: false,
                                        animationLoop: false,
                                        slideshow: false,
                                        itemWidth: 100,
                                        itemMargin: 5,
                                        asNavFor: '#slider'
                                      });

                                      $('#slider').flexslider({
                                        animation: "slide",
                                        controlNav: false,
                                        animationLoop: false,
                                        slideshow: false,
                                        sync: "#carousel"
                                      });
                                });
                            </script>

                        <?php
                        }
                        // Single image
                        else if($content->thumb_desktop){
                            $picture = @$content->thumb_desktop ? $content->thumb_desktop : "";
                            if( $mobileDetect->isMobile() ){
                                $picture = $content->thumb_mobile;
                            }else if($mobileDetect->isTablet()){
                                $picture = $content->thumb_tablet;
                            }
                            $img_alt    = @$gallery->alt;
                            if($picture!=""){
                                ?>
                                    <div class="carousel-inner">
                                        <div class="item active"><img src="<?php echo $picture;?>" alt="<?php echo $img_alt;?>"></div>
                                    </div><!-- carousel-inner -->
                                <?php
                            }
                        }

                    ?>
                    
                    <div class="detail">
                        <p><?php echo $content->long_des;?></p>
                    </div>
                    
                    <?php
                        $this->load->view("include/social_share_button", array(
                            "url" => current_url(),
                            "title" => $_TITLE,
                            "caption" => $_DESC,
                            "image" => $_IMAGE
                        ));
                    ?>

                    <nav>
                        <ul class="pager">
                            <li class="previous <?php echo !$content->prev ? 'disabled':'';?>">
                                <a href="<?php echo $content->prev ? _site_url( $this->cmd."/detail/{$content->prev}") : 'javascript:void(0);';?>">
                                    <span class="glyphicon glyphicon-triangle-left"></span> ย้อนกลับ
                                </a>
                            </li>
                            <li class="next <?php echo !$content->next ? 'disabled':'';?>">
                                <a href="<?php echo $content->next ? _site_url( $this->cmd."/detail/{$content->next}") : 'javascript:void(0);';?>">
                                    ถัดไป <span class="glyphicon glyphicon-triangle-right"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>                    
                    
                </div><!-- /.content -->
                </div><!-- /data-sr -->
                
            </div>
        	<div class="col-md-4 col-xs-12">
            
            	<div data-sr>
                <div class="sidebar">
                	
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#tab_facebook" data-toggle="pill"><i class="facebook"></i></a></li>
                        <li><a href="#tab_instagram" data-toggle="pill"><i class="instagram"></i></a></li>
                        <li><a href="#tab_youtube" data-toggle="pill"><i class="youtube"></i></a></li>
                    </ul>
                    
                    <div class="box-shadow">

                        <?php
                            $this->load->view("include/view_detail_social_tab");
                        ?>
                        <!--/.tab-content -->
                        
                        <div class="related-content">
                            <img src="assets/images/article/all_article.png" alt="บทความอื่นๆ">
                            <ul>
                                <?php
                                    $otherContent = getPurra()->lifestyle(0, 5);
                                    if( $otherContent ){
                                        $otherContent = json_decode($otherContent);
                                        $len = count($otherContent->data->lifestyle);
                                        for( $i=0;  $i<$len&&$i<3;  $i++ ){
                                            $item   = $otherContent->data->lifestyle[$i];
                                            $url    = _site_url($this->cmd."/detail/{$item->id}");
                                            if( !$item->thumb_desktop ){
                                                $item->thumb_desktop = base_url("public/image/thumb-no-img.jpg");
                                            }
                                            $image  = $item->thumb_desktop;
                                            if( $mobileDetect->isMobile() ){
                                                $image = $item->thumb_mobile;
                                            }else if($mobileDetect->isTablet()){
                                                $image = $item->thumb_tablet;
                                            }
                                        ?>   
                                            <li>
                                                <a href="<?php echo $url;?>">
                                                    <img src="<?php echo $image;?>" alt="<?php echo $item->alt;?>" />
                                                </a>
                                                <p><?php echo utf8_substr(strip_tags($item->name), 0, 60);?></p>
                                            </li>
                                        <?php
                                        }
                                    }
                                ?>
                            </ul>
                            <div class="view-all">
                            	<a href="<?php echo _site_url("lifestyle/showlist");?>">view all <span class="glyphicon glyphicon-plus-sign"></span></a>
                            </div>
                        </div><!--/.related-content -->
                    
                    </div>
                    
                </div><!--/.sidebar -->
                </div><!-- /data-sr -->
                
            </div>
        </div>
        
    
        <div class="move_up"></div>
    </div><!-- /.container -->
    

</div>
