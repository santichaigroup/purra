<?php
if( $rs  &&  $rs->code==200 ){
    $len        = is_array($rs->data->lifestyle) ? count($rs->data->lifestyle) : 0;
    for( $i=0;  $i<$len;  $i++ ){
        $item   = $rs->data->lifestyle[$i];
        
        $image = @$item->thumb_desktop ? $item->thumb_desktop : "";
        if( $mobileDetect->isMobile() ){
            $image = $item->thumb_mobile;
        }else if($mobileDetect->isTablet()){
            $image = $item->thumb_tablet;
        }
        if( $image=="" ){
            $image = base_url("public/image/thumb-no-img.jpg");
        }
        $img_alt = $item->alt;
    ?>
        <li>
            <div class="thumbnail">
                <a href="<?php echo _site_url("lifestyle/detail/{$item->id}");?>">
                    <img src="<?php echo $image;?>" alt="<?php echo $img_alt;?>" />
                </a>
                <p><?php echo $item->name;?></p>
                <a href="<?php echo _site_url("lifestyle/detail/{$item->id}");?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
            </div>
        </li>
    <?php
    }
}