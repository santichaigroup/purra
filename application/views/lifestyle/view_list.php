<div class="mainContent">

    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->
    

    <div class="container">
    
    
        <div data-sr>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="half">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo _site_url("home")?>">Home</a></li>
                            <li><a>Lifestyle</a></li>
                        </ol>
                    </div>
                    <div class="half">
                        <div class="categories">
                            <form action="" method="post">
                                <label>เลือกประเภท</label>
                                <div class="form-group">
                                    <select name="type" 
                                            id="type" 
                                            title="All Categories"
                                            onchange="window.location='<?php echo _site_url("lifestyle");?>/?cid='+ this.value;">
                                        <option value="">All Categories</option>
                                        <?php
                                            $category     = getPurra()->lifestyleCategory();
                                            $category     = @json_decode($category);
                                            if( $category  &&  $category->code==200 ){
                                                $len    = count( $category->data->category );
                                                for( $i=0;  $i<$len;  $i++ ){
                                                    $item = $category->data->category[$i];
                                                    $selected = @$_REQUEST['cid']==$item->id ? ' selected="selected" ' : '';
                                                ?>
                                                    <option value="<?php echo $item->id;?>" <?php echo $selected;?> ><?php echo $item->name;?></option>
                                                <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /data-sr -->
        
        
        <div class="content">
            <div class="row">
            
                <ul id="list-items" class="list-items">
                    
                    <?php
                        $len    = 0;
                        $total  = 0;
                        if( $rs  &&  $rs->code==200 ){
                            $len        = is_array($rs->data->lifestyle) ? count($rs->data->lifestyle) : 0;
                            $total      = $rs->data->quantity;
                            for( $i=0;  $i<$len;  $i++ ){
                                $item   = $rs->data->lifestyle[$i];
                                
                                $image = $item->thumb_desktop;
                                if( $mobileDetect->isMobile() ){
                                    $image = $item->thumb_mobile;
                                }else if( $mobileDetect->isTablet() ){
                                    $image = $item->thumb_tablet;
                                }
                                if( $image=="" ){
                                    $image = base_url("public/image/thumb-no-img.jpg");
                                }
                                
                                //$img_alt = utf8_substr($item->name, 0, 50);
                                $img_alt = @$item->alt;
                            ?>
                                <li>
                                    <div class="thumbnail">
                                        <a href="<?php echo _site_url("lifestyle/detail/{$item->id}");?>">
                                            <img src="<?php echo $image;?>" alt="<?php echo $img_alt;?>" />
                                        </a>
                                        <p><?php echo $item->name;?></p>
                                        <a href="<?php echo _site_url("lifestyle/detail/{$item->id}");?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                                    </div>
                                </li>
                            <?php
                            }
                        }
                    ?>
                    
                </ul>
                
                <?php
                    if( $len>0  &&  $len>=$limit  &&  $len<$total ){
                    ?>
                        <div class="col-xs-12 loadmore">
                            <div id="loadMore"></div>
                        </div>
                    <?php
                    }
                ?>
                
            </div>
        </div><!-- /#content -->
        
    
        <?php
            if( $len>0 ){
            ?>
                <div class="move_up"></div>
            <?php
            }
        ?>
        
    </div><!-- /.container -->
    

</div>
<script>
    $(function(){
        $('#loadMore').click(function(){
            $.post( "<?php echo _site_url("lifestyle/loadmore")?>",{
                        page : Math.ceil($('ul#list-items > li').toArray().length/<?php echo $limit;?>)+1,
                        limit : '<?php echo $limit;?>',
                        category : '<?php echo @$_REQUEST['cid'];?>'
                    }, function(html){
                        if( html=="" ){
                            $('#loadMore').hide();
                        }else{
                            $('ul#list-items').append(html);
                            if($('ul#list-items > li').toArray().length >= <?php echo $total;?>){
                                $('#loadMore').fadeOut();
                            }else 
                                if( Math.ceil($('ul#list-items > li').toArray().length % <?php echo $limit;?>) !=0 ){
                                $('#loadMore').fadeOut();
                            }
                        }
                    });
        });
    });
</script>