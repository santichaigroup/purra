<div class="tab-content">
    <div class="tab-pane active" id="tab_facebook" >
        <?php
            $this->load->view("include/facebook_fanpage_plugin", array(
                'height' => 500
            ));
        ?>
    </div>
    <div class="tab-pane" id="tab_instagram">
        <center>
            <br />
            <img src="image/loading.gif" />
        </center>
    </div>
    <div class="tab-pane" id="tab_youtube">
        <center>
            <br />
            <img src="image/loading.gif" />
        </center>
    </div>
</div>
<script>
    $(function(){
        $.post( "<?php echo _site_url("view/vdoFromYoutubeLayout")?>",
                function(html){
                    $(".tab-content #tab_youtube").html(html);
                });
                
        $.post( "<?php echo _site_url("view/instagram")?>",
                function(html){
                    $(".tab-content #tab_instagram").html(html);
                });        
                
        $(window).resize(function(){
            var div_fb_pluginFanpage = $(".box-shadow.feed-facebook");
            loadFacebookFanpagePlugin(
                    div_fb_pluginFanpage, 
                    div_fb_pluginFanpage.width(), 
                    div_fb_pluginFanpage.height()
            );
        });        
    });
</script>