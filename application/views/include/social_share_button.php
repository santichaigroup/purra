<div class="social-share">
    <!--<img src="assets/images/article/social_share.png">-->
    <?php
        $_url       = @$url ? $url : "" ;
        $_title     = @$title ? $title : "" ;
        $_caption   = @$caption ? $caption : "" ;
    ?>
    <div class="social-share-container">
        <div class="btn-share facebook"
              style="">
            <div class="fb-share-button" 
                data-href="<?php echo $_url;?>" 
               data-layout="button_count"
               style="clear: both;">
           </div>
        </div>

        <div class="btn-share twitter">
            <a href="https://twitter.com/share" 
               class="twitter-share-button"{count} 
               data-url="<?php echo $_url;?>" 
               data-text="<?php echo trim(utf8_substr($_caption, 0, 100) );?>">Tweet</a>
        </div>

        
        <?php
            if( $mobileDetect->isMobile() || $mobileDetect->isTablet() ){
                $data = array(
                    "pc"    => false,
                    "lang"  => 'en',
                    "type"  => "a",
                    "text"  => trim(utf8_substr(preg_replace('/\s\s+/', ' ', $_caption), 0, 100)),
                    "withUrl" => true
                );
            ?>
                <div class="btn-share line" >
                    <span>
                        <script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411" ></script>
                        <script type="text/javascript">
                            new media_line_me.LineButton(<?php echo json_encode($data);?>);
                        </script>
                    </span>
                </div>
            <style>
                .social-share-container{ width: 300px !important; }
            </style>
            <?php
            }
        ?>
        
    </div>
    <div class="clear"></div>

</div>