<?php
    $footer     = getPurra()->footer();
    $footer     = json_decode($footer);
    $products   = null;
    $lat        = '13.7912442'; //Boonrawd Trading Co.,LTD.
    $lng        = '100.5112307';//Boonrawd Trading Co.,LTD.
    $contact    = null;
    $termAndConditionlUrl = "";
    $privacyUrl = "";
    $logo       = "";
    $contactUsURL = "javascript:void(1);";
    if( @$footer->code==200 ){
        $products   = $footer->data->product;
        $lat        = $footer->data->location->latitude;
        $lng        = $footer->data->location->longitude;
        $contact    = $footer->data->contact;
        $termAndConditionlUrl = $footer->data->term_condition_url;
        $logo       = $footer->data->logo;
    }
    
    $contactUsURL   = @$footer->data->contactus ? $footer->data->contactus : $contactUsURL;
    
    $header         = getPurra()->header();
    $header         = json_decode($header);
    
//    $ln_facebook    = $header && $header->data->social->facebook ? $header->data->social->facebook : "";
//    $ln_youtube     = $header && $header->data->social->youtube ? $header->data->social->youtube : "";
    $socials            = getPurra()->social(true);
    $social_youtube     = @$socials->data->bottom->youtube[0] ? $socials->data->bottom->youtube[0]    : "";
    $social_facebook    = @$socials->data->bottom->facebook[0] ? $socials->data->bottom->facebook[0]    : "";
    $social_instagram   = @$socials->data->bottom->instagram[0] ? $socials->data->bottom->instagram[0]  : "";
    $social_twitter     = @$socials->data->bottom->twitter[0] ? $socials->data->bottom->twitter[0]      : "";
    $social_googleplus  = @$socials->data->bottom->googleplus[0] ? $socials->data->bottom->googleplus[0] : "";
    $social_email       = @$socials->data->bottom->email[0] ? $socials->data->bottom->email[0]          : "";
?>
<footer>
	<div class="footer-top">
    	<div class="container">
            <div class="row">
                
                <div class="col-md-8 col-xs-6 newsleter hidden-xs">
                    <form class="form-newsleter" action="<?php echo _site_url("api/newsLetter");?>" method="post" onsubmit="return sendNewsLetter();">
                        <div class="form-group">
                            <label>NEWSLETTER</label>
                            <input type="text" class="form-control" name="newsleter_email" id="newsleter_email" placeholder="your email">
                        </div>
                        <button type="submit" class="btn btn-default input-group-addon" >SUBMIT</button>
                    </form>
                </div>
                
                <div class="col-md-8 col-xs-6 newsleter hidden-lg hidden-md hidden-sm">
                    <form class="form-newsleter" action="<?php echo _site_url("api/newsLetter");?>" method="post" onsubmit="return sendNewsLetter();">
                        <div class="form-group">
                            <label style="margin: 3px 10px 3px 0;
                                            font-size: 16px;">NEWSLETTER</label>
                            <input type="text" 
                                   class="form-control" 
                                   name="newsleter_email" 
                                   id="newsleter_email" 
                                   placeholder="your email"
                                   style="width: 110px;" />
                        </div>
                        <button type="submit" 
                                class="btn btn-default input-group-addon"
                                style="width: 70px;">SUBMIT</button>
                    </form>
                </div>
                
                
                <div class="col-md-4 col-xs-6 follow">
                    <p>SHARE</p>
                    <a href="javascript:void(0);" onclick="shareFaceBook();"><i class="fa fa-facebook"></i></a>
                    <?php
                        if( @$social_youtube->url ){
                        ?>
                            <a href="<?php echo $social_youtube->url;?>" target="_blank"><i class="fa fa-youtube"></i></a>
                        <?php
                        }
                        
                        if( @$social_instagram->url ){
                        ?>
                            <a href="<?php echo $social_instagram->url;?>" target="_blank"><i class="fa fa-instagram"></i></a>
                        <?php
                        }
                        
                        if( @$social_twitter->url ){
                        ?>
                            <script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
                            <!--<a href="https://twitter.com/intent/tweet?in_reply_to=463440424141459456">Reply</a>-->
                            <!--<a href="<?php echo $social_twitter->url;?>" ><i class="fa fa-twitter"></i></a>-->
                            <a href="https://twitter.com/intent/tweet?&text=<?php echo urlencode($_ENV['meta_description']);?>&url=<?php echo current_url();?>"><i class="fa fa-twitter"></i></a>
                        <?php
                        }
                        
                        if( @$social_googleplus->url ){
                        ?>
                            <!--<script src="https://apis.google.com/js/platform.js" async defer></script>-->
                            <a href="<?php echo $social_googleplus->url;?>" 
                               target="_blank" 
                               class="g-interactivepost" >
                                <i class="fa fa-google-plus"></i>
                            </a> 
                        <?php
                        }
                        
                        if( @$social_email->url ){
                        ?>
                            <a href="mailto:<?php echo $social_email->url;?>" ><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                        <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div><!-- / footer .top -->
    
	<div class="footer-site">
    	<div class="container">
        
            <div class="nav-desktop">
            	<!--<ul>
                	<li><a href="<?php //echo _site_url("abooutus");?>"><b>about us</b></a></li>
                </ul>-->
                
            	<?php
                    if( $products!=null  && is_array($products)  &&  count($products)>0 ){
                        foreach( $products AS $product ){
                        ?>
                            <ul>
                                <li><a><b><?php echo @$product->category?$product->category:"";?></b></a></li>
                                <?php
                                    if( @$product->brand  && is_array($product->brand)  && count($product->brand)>0 ){
                                       $brands  = $product->brand;
                                       foreach($brands AS $brand){
                                       ?>
                                            <li><a href="<?php echo $brand->url;?>" target="_blank"><?php echo $brand->name;?></a></li>
                                       <?php
                                       }
                                    }
                                ?>
                            </ul>
                        <?php
                        }
                    }
                ?>
            	
            	<ul>
                    <li><a href="<?php echo $contactUsURL;?>" target="_blank"><b><?php echo langs('CONTACT US');?></b></a></li>
                </ul>
            </div>
            <div class="address">
            	<a href="http://singha.com/" target="_blank"><img src="<?php echo $logo;?>" class="logo"></a>
                <p>
                    <?php echo $contact?$contact->company_name : "";?><br>
                    <?php echo $contact?$contact->company_address : "";?><br>
                    Tel: <?php echo $contact?$contact->company_phone : "";?><br>
                    Fax: <?php echo $contact?$contact->company_fax : "";?><br>
                    <a href="http://maps.google.com?q=<?php echo $lat;?>,<?php echo $lng;?>" target="_blank"><b>Singha Corporation Map</b> <i class="fa fa-map-marker"></i></a>
                </p>
            </div>
        </div>
    </div><!-- / .footer-site -->
    
	<div class="footer-site mobile">
    	<div class="container">
        
            <div class="nav-mobile">
                <ul>
                    <li><a href="<?php echo _site_url("home");?>">HOME</a></li>
                    <!--<li><a>ABOUT US</a>-->
                    <?php
                        if( $products!=null  && is_array($products)  &&  count($products)>0 ){
                            foreach( $products AS $product ){
                            ?>
                        
                                <li class="nav-sub">
                                    <a>
                                        <i class="icon_beer" style="background-image: url(<?php echo $product->logo;?>);"></i>
                                        <?php echo @$product->category?$product->category:"";?>
                                    </a>
                                    <ul>
                                        <?php
                                            if( @$product->brand  && is_array($product->brand)  && count($product->brand)>0 ){
                                               $brands  = $product->brand;
                                               foreach($brands AS $brand){
                                               ?>
                                                    <li>
                                                        <a href="<?php echo $brand->url;?>" target="_blank">
                                                            <span><img src="<?php echo $brand->logo;?>"></span> 
                                                            <?php echo $brand->name;?>
                                                        </a>
                                                    </li>
                                               <?php
                                               }
                                            }
                                        ?>
                                    </ul>
                                </li>
                            <?php
                            }
                        }
                    ?>    
                    
                    <li><a href="http://boonrawd.co.th/main.html" target="_blank">BOONRAWD.CO.TH</a></li>
                    <li><a href="http://www.singhaonlineshop.com/2015" target="_blank">SINGHA ONLINE SHOP</a></li>
                </ul>
            </div>
            
            <div class="address"> 
                <a href="http://singha.com/" target="_blank"><img src="<?php echo $logo;?>" class="logo"></a>
                <p>
                    <?php echo $contact?$contact->company_name : "";?><br>
                    <?php echo $contact?$contact->company_address : "";?><br>
                    Tel: <?php echo $contact?$contact->company_phone : "";?>  Fax: <?php echo $contact?$contact->company_fax : "";?>
                </p>
                <a href="tel:<?php echo $contact?$contact->company_phone : "";?>"><i class="icon_call"></i></a>
                <a href="http://maps.google.com?q=<?php echo $lat;?>,<?php echo $lng;?>" target="_blank" ><i class="icon_directions"></i></a>
            </div>
            
        </div>
    </div><!-- / .footer-site.mobile -->
    
    <div class="footer-copyright">
    	<div class="container">
            <div class="row">
            	<div class="col-xs-12 col-md-6 col-other">
                    <ul>
                        <li><a href="<?php echo $termAndConditionlUrl?>" target="_blank">Terms & Conditions</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6 col-copyright">
                    <p>© COPYRIGHTS 2014 SINGHA CORPORATION CO., LTD. ALL RIGHTS RESERVED.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- / .footer-copyright -->
    
    
    
    <div class="modal fade popup" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-bordered">
                    <div class="modal-body" style="padding:50px;">
                        <span class="modal-body-content"></span><br>
                        <img src="assets/images/contact/back_to_home.png" class="center-block" data-dismiss="modal">
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    
    
</footer>
<script>
    function sendNewsLetter(){
        $(".modal-body-content").html('');
        $.post("<?php echo _site_url("api/newsLetter");?>", {
            newsleter_email : $(".form-newsleter input[name=newsleter_email]").val()
        }, function(data){
            data = $.parseJSON(data);
            if(data==null  ||  data.code!=200){
                $(".modal-body-content").html('<p>'+ data.message+'</p>');
            }else{
                $(".form-newsleter input[name=newsleter_email]").text("");
                $(".modal-body-content").html('<h3>ขอบคุณค่ะ</h3><p>ทางทีม เพอร์ร่า ได้รับข้อมูลของคุณเรียบร้อยแล้ว</p>');
            }
            $("#modal-dialog").modal('show');
        });
        return false;
    }
</script>