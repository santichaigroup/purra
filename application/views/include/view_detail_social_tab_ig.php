<div class="instagram-container">
    <?php
        $instagram      = getPurra()->instagram();
        $instagram      = json_decode($instagram);
        if( $instagram  &&  $instagram->code=='200' ){
            $len = count($instagram->data);
            
            $_source = null;
            if( @$instagram->data->source->{'custom-upload'}->images ){
                $_source = &$instagram->data->source->{'custom-upload'};
            }else if( @$instagram->data->source->hashtag->images ){
                $_source = &$instagram->data->source->hashtag;
            }
            
            $hashtag        = @$instagram->data->source->hashtag->hashtags ? 
                                $instagram->data->source->hashtag->hashtags 
                                : "";
            
            if( $_source!=null  &&  $_source->images ){
                $len        = count($_source->images);
                for( $i=0;  $i<$len;  $i++ ){
                    $item = $_source->images[$i];
                    $name = @$item->name ? $item->name : "" ;
                ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="thumbnail">
                                <img src="<?php echo $item->photo_standard;?>" style="width:100%;">
                                <p class="p-first"><?php echo "{$hashtag}{$name}";?></p>
                            </div>
                        </div>
                    </div>
                <?php
                }

            }
            
        }
    ?>
</div>