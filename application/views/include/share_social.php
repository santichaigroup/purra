<?php
    $socials            = getPurra()->social(true);
    $social_youtube     = @$socials->data->side->youtube[0] ?  $socials->data->side->youtube[0]    : "";
    $social_facebook    = @$socials->data->side->facebook[0] ?  $socials->data->side->facebook[0]    : "";
    $social_instagram   = @$socials->data->side->instagram[0] ? $socials->data->side->instagram[0]  : "";
    $social_twitter     = @$socials->data->side->twitter[0] ?    $socials->data->side->twitter[0]      : "";
    $social_googleplus  = @$socials->data->side->googleplus[0] ?  $socials->data->side->googleplus[0] : "";
    $social_email       = @$socials->data->side->email[0] ?  $socials->data->side->email[0]        : "";
    $social_shop       = @$socials->data->side->shop[0] ?  $socials->data->side->shop[0]        : "";
?>
<div class="share">
    <ul class="share-social">
        <?php
            if( @$social_facebook->url ){
            ?>
                <li>
                    <a href="<?php echo $social_facebook->url;?>" target="_blank">
                        <?php
                            if( @$social_facebook->logo ){
                            ?>
                                <img src="<?php echo $social_facebook->logo;?>" alt="<?php echo @$social_facebook->alt;?>" />
                            <?php
                            }else{
                            ?>
                                <img src="assets/images/icons/share_facebook.png">
                            <?php
                            }
                        ?>
                    </a>
                </li>
            <?php
            }
            
            if( @$social_twitter->url ){
            ?>
                <li>
                    <a href="<?php echo $social_twitter->url;?>" target="_blank">
                        <?php
                            if( @$social_twitter->logo ){
                            ?>
                                <img src="<?php echo $social_twitter->logo;?>" alt="<?php echo @$social_twitter->alt;?>"  />
                            <?php
                            }else{
                            ?>
                                <img src="assets/images/icons/share_twitter.png">
                            <?php
                            }
                        ?>
                    </a>
                </li>
            <?php
            }
            
            if( @$social_instagram->url ){
            ?>
                <li>
                    <a href="<?php echo $social_instagram->url;?>" target="_blank">
                        <?php
                            if( @$social_instagram->logo ){
                            ?>
                                <img src="<?php echo $social_instagram->logo;?>" alt="<?php echo @$social_instagram->alt;?>"  />
                            <?php
                            }else{
                            ?>
                                <img src="assets/images/icons/share_instagram.png">
                            <?php
                            }
                        ?>
                    </a>
                </li>
            <?php
            }
            
            if( @$social_youtube->url ){
            ?>
                <li>
                    <a href="<?php echo $social_youtube->url;?>" target="_blank">
                        <?php
                            if( @$social_youtube->logo ){
                            ?>
                                <img src="<?php echo $social_youtube->logo;?>" alt="<?php echo @$social_youtube->alt;?>"  />
                            <?php
                            }else{
                            ?>
                                <img src="assets/images/icons/share_youtube.png">
                            <?php
                            }
                        ?>
                    </a>
                </li>
            <?php
            }
            
            if( @$social_googleplus->url  &&  @$social_googleplus->logo ){
            ?>
                <li>
                    <a href="<?php echo $social_googleplus->url;?>" target="_blank">
                        <img src="<?php echo $social_googleplus->logo;?>" alt="<?php echo @$social_googleplus->alt;?>"  />
                    </a>
                </li>
            <?php
            }
            
            if( @$social_email->url  &&  @$social_email->logo ){
            ?>
                <li>
                    <a href="mailto:<?php echo $social_email->url;?>" >
                        <img src="<?php echo $social_email->logo;?>" alt="<?php echo @$social_email->alt;?>"  />
                    </a>
                </li>
            <?php
            }
            
            if( $this->uri->segment(3)!="contact" ){
                if( @$social_shop->url  &&  @$social_shop->logo ){
                ?>
                    <li>
                        <a href="<?php echo $social_shop->url;?>" target="_blank">
                            <img src="<?php echo $social_shop->logo;?>" alt="<?php echo @$social_shop->alt;?>"  />
                        </a>
                    </li>
                <?php
                }
            }
            
        ?>
    </ul>
</div>