<?php
    $header = getPurra()->header();
    $header = json_decode($header);
    
    $ln_singha      = @$header->data->link->singha ? $header->data->link->singha : null ;
    $ln_boonrawd    = @$header->data->link->boonrawd ? $header->data->link->boonrawd : null ;
    $ln_shop        = @$header->data->link->shop ? $header->data->link->shop : null ;
    
    $socials            = getPurra()->social(true);
    $social_youtube     = @$socials->data->top->youtube[0] ?  $socials->data->top->youtube[0]    : "";
    $social_facebook    = @$socials->data->top->facebook[0] ?  $socials->data->top->facebook[0]    : "";
    $social_instagram   = @$socials->data->top->instagram[0] ? $socials->data->top->instagram[0]  : "";
    $social_twitter     = @$socials->data->top->twitter[0] ?    $socials->data->top->twitter[0]      : "";
    $social_googleplus  = @$socials->data->top->googleplus[0] ?  $socials->data->top->googleplus[0] : "";
    $social_email       = @$socials->data->top->email[0] ?  $socials->data->top->email[0]        : "";
?>
<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
        
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="menu-bar"></span>
                </button>
                
                <a class="navbar-brand" href="<?php echo _site_url("home");?>"><img src="assets/images/logo_purra.png"></a>
                
                <ul class="nav navbar-nav">
                    <?php
                        if( $ln_singha ){
                            if( !$ln_singha->url ){
                                $ln_singha->url = "javascript:void(1);";
                            }
                        ?>
                            <li>
                                <a href="<?php echo $ln_singha->url;?>" target="_blank">
                                    <?php
                                        if( $ln_singha->img!="" ){
                                        ?>
                                            <i class="logo_singha_small"
                                                style="background-image: url(<?php echo $ln_singha->img;?>);
                                                         background-position: 50% 50%;
                                                         background-repeat: no-repeat;
                                                         background-size: contain;"></i> 
                                        <?php
                                        }
                                    ?>
                                    <?php echo $ln_singha->text;?>
                                </a>
                            </li>
                        <?php
                        }
                        
                        if( $ln_boonrawd ){
                            if( !$ln_boonrawd->url ){
                                $ln_boonrawd->url = "javascript:void(1);";
                            }
                        ?>
                            <li>
                                <a href="<?php echo $ln_boonrawd->url;?>" target="_blank">
                                    <?php
                                        if( $ln_boonrawd->img!="" ){
                                        ?>
                                            <i class="logo_singha_small"
                                                style="background-image: url(<?php echo $ln_boonrawd->img;?>);
                                                         background-position: 50% 50%;
                                                         background-repeat: no-repeat;
                                                         background-size: contain;"></i> 
                                        <?php
                                        }
                                    ?>
                                    <?php echo $ln_boonrawd->text;?>
                                </a>
                            </li>
                        <?php
                        }
                        
                        if( $ln_shop ){
                            if( !$ln_shop->url ){
                                $ln_shop->url = "javascript:void(1);";
                            }
                        ?>
                            <li>
                                <a href="<?php echo $ln_shop->url;?>" target="_blank">
                                    <?php
                                        if( $ln_shop->img!="" ){
                                        ?>
                                            <i class="logo_singha_small"
                                                style="background-image: url(<?php echo $ln_shop->img;?>);
                                                         background-position: 50% 50%;
                                                         background-repeat: no-repeat;
                                                         background-size: contain;"></i> 
                                        <?php
                                        }
                                    ?>
                                    <?php echo $ln_shop->text;?>
                                </a>
                            </li>
                        <?php
                        }
                    ?>
                    <li class="icons">
                        <?php
                            if( $social_facebook ){
                                $url    = @$social_facebook->url ? $social_facebook->url : "javascript:void(0);";
                            ?>
                                <a href="<?php echo $url;?>" target="_blank" style="padding:0px;">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            <?php
                            }
                            
                            if( $social_instagram ){
                                $url    = @$social_instagram->url ? $social_instagram->url : "javascript:void(0);";
                            ?>
                                <a href="<?php echo $url;?>" target="_blank" style="padding:0px;">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            <?php
                            }
                            
                            if( $social_youtube ){
                                $url    = @$social_youtube->url ? $social_youtube->url : "javascript:void(0);";
                            ?>
                                <a href="<?php echo $url;?>" target="_blank" style="padding:0px;">
                                    <i class="fa fa-youtube"></i>
                                </a>
                            <?php
                            
                            }
                            
                            if( $social_twitter ){
                                $url    = @$social_twitter->url ? $social_twitter->url : "javascript:void(0);";
                            ?>
                                <a href="<?php echo $url;?>" target="_blank" style="padding:0px;">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            <?php
                            
                            }
                            
                            if( $social_googleplus ){
                                $url    = @$social_googleplus->url ? $social_googleplus->url : "javascript:void(0);";
                            ?>
                                <a href="<?php echo $url;?>" target="_blank" style="padding:0px;">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            <?php
                            
                            }
                            
                            if( $social_email ){
                                $url    = @$social_email->url ? $social_email->url : "javascript:void(0);";
                            ?>
                                <a href="mailto:<?php echo $url;?>"  style="padding:0px;">
                                    <!--<i class="fa fa-mail-reply"></i>-->
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                </a>
                            <?php
                            
                            }
                            
                            
                        ?>
                    </li>
                    <li class="icons"><a><i id="open-search" class="icon-search"></i></a></li>
                    <li class="language">
                        <?php
                            $lang = getLang();
                        ?>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo strtoupper($lang);?> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <?php
                                    $lang_url   = "";
                                    $lang_txt   = "";
                                    
                                    $refer          = "";
                                    $controllerName = $this->router->fetch_class();
                                    $currentUrl     = current_url();
                                    if( strstr($currentUrl, $controllerName) ){
                                        $_tmp       = explode("{$controllerName}", $currentUrl);
                                        if(count($_tmp)>1 ){
                                            $suffixUrl  = $_tmp[count($_tmp)-1];
                                            if( substr($suffixUrl, 0, 1)=="/" ){
                                                $suffixUrl = substr($suffixUrl, 1);
                                            }
                                            $refer      = "?refer={$controllerName}/{$suffixUrl}";
                                        }
                                    }
                                    
                                    switch(strtolower($lang) ){
                                        case 'en' :
                                            $lang_url = site_url("language/changeLanguage/th{$refer}");
                                            $lang_txt = "TH";
                                            break;
                                        case 'th' :
                                        default:
                                            $lang_url = site_url("language/changeLanguage/en{$refer}");
                                            $lang_txt = "EN";
                                            break;
                                    }
                                ?>
                                <a href="<?php echo $lang_url;?>"><?php echo $lang_txt;?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
            </div><!-- /.navbar-header -->
            
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <?php
                        $menuName = $this->uri->segment(2);
                    ?>
                    <li class="<?php echo $menuName==""||$menuName=="home"?"active":"";?>"><a href="<?php echo _site_url("home");?>">Home</a></li>
                    <li class="<?php echo $menuName=="lifestyle"?"active":"";?>"><a href="<?php echo _site_url("lifestyle");?>">Lifestyle</a></li>
                    <li class="<?php echo $menuName=="promotion"?"active":"";?>"><a href="<?php echo _site_url("promotion");?>">Promotions & Activities</a></li>
                    <li class="<?php echo $menuName=="product"?"active":"";?>"><a href="<?php echo _site_url("product");?>">Product</a></li>
                    <li class="<?php echo $menuName=="contact"?"active":"";?>"><a href="<?php echo _site_url("contact");?>">CONTACT US<?php //echo langs('CONTACT US');?></a></li>
                </ul>
            </div><!-- /.nav-collapse -->
            
        </div>
    </nav>
    
    <div class="container">
        <form class="form-search" action="<?php echo _site_url("search")?>" method="get" onsubmit="return onSearch();">
            <input type="text" class="form-control" name="search" placeholder="search" />
        </form>
    </div><!-- /.form-search -->
    
</header>