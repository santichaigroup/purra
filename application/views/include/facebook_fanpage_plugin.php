<?php
    $w = @$width ? ' data-width="'.$width.'"  ' : "";
    $h = @$height ? ' data-height="'.$height.'"  ' : "";
    $social = getPurra()->social();
    $social = json_decode($social);
?>
<div class="fb-page" 
        data-href="https://www.facebook.com/<?php echo $social->data->content->facebook;?>/" 
        data-tabs="timeline" 
        <?php echo $w;?>
        <?php echo $h;?>
        data-small-header="false" 
        data-adapt-container-width="true" 
        data-hide-cover="false" 
        data-show-facepile="true">
        <div class="fb-xfbml-parse-ignore">
            <blockquote cite="https://www.facebook.com/<?php echo $social->data->content->facebook;?>"><a href="https://www.facebook.com/<?php echo $social->data->content->facebook;?>">Purra</a></blockquote>
        </div>
</div>