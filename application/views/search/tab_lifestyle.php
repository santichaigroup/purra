<div role="tabpanel" class="tab-pane" id="lifestyle">
                        
    <h3>ผลการค้นหา <span>“<?php echo $searchText;?>”</span></h3>
    <ul class="list-items-results">
        <?php
            $len = 0;
            if( @$searchResult->data->result->lifestyle->contents ){
                $len        = count($searchResult->data->result->lifestyle->contents);
                if( $len > 0 ){
                    foreach($searchResult->data->result->lifestyle->contents  AS $row){
                        $image  = "";
                        if( $mobileDetect->isMobile() ){
                            $image = $row->thumb_mobile;
                        }else if( $mobileDetect->isTablet() ){
                            $image = $row->thumb_tablet;
                        }else{
                            $image = $row->thumb_desktop;
                        }
                        if( $image=="" ){
                            $image = base_url("public/image/thumb-no-img.jpg");
                        }
                        $url        = _site_url("lifestyle/detail/". $row->id);
                    ?>
                        <li>
                            <a href="<?php echo $url;?>">
                                <div class="thumbnail">
                                    <img src="<?php echo $image;?>" alt="<?php echo @$row->alt;?>" />
                                </div>
                                <div class="caption">
                                    <h4><?php echo @$row->name ? $row->name : "";?></h4>
                                    <p><?php echo $url;?></p>
                                    <p>
                                        <?php 
                                            echo utf8_substr(strip_tags($row->short_des), 0, 250);
                                        ?>
                                    </p>
                                </div>
                            </a>
                            
                        </li>
                    <?php
                    }
                }
            }
        ?>
    </ul>
    <?php
        if( $len > 0 ){
        ?>
            <div class="col-xs-12 loadmore">
                <div class="btn btn-loadmore">LOAD MORE ...</div>
            </div>
        <?php
        }
    ?>
    
    <input type="hidden" name="page" value="1" />
    
    <script>
        <?php
            $tabId = "lifestyle";
        ?>
        $(function(){
            $("div.tab-pane#<?php echo $tabId;?> .btn-loadmore").click(function(){
                $("div.tab-pane#<?php echo $tabId;?> input[name=page]").val( parseInt($("div.tab-pane#<?php echo $tabId;?> input[name=page]").val()) + 1 );
                $.post( "<?php echo _site_url("search/loadmore_tabLifestyle");?>",
                {
                    limit : <?php echo $limit;?>,
                    page : $("div.tab-pane#<?php echo $tabId;?> input[name=page]").val(),
                    query : '<?php echo $searchText;?>'
                }, function(data){
                    if(data!=""){
                        $("div.tab-pane#<?php echo $tabId;?> ul.list-items-results").append(data);
                    }else{
                        $("div.tab-pane#<?php echo $tabId;?> .loadmore").hide();
                    }
                });
            });
        });
    </script>

</div>