<?php
    if( @$rs ){
        $searchResult = $rs;
        
        if( @$searchResult->data->result->promotion->contents ){
            $len        = count($searchResult->data->result->promotion->contents);
            if( $len > 0 ){
                foreach($searchResult->data->result->promotion->contents  AS $row){
                    $image  = "";
                    if( $mobileDetect->isMobile() ){
                        $image = $row->thumb_mobile;
                    }else if( $mobileDetect->isTablet() ){
                        $image = $row->thumb_tablet;
                    }else{
                        $image = $row->thumb_desktop;
                    }
                    if( $image=="" ){
                        $image = base_url("public/image/thumb-no-img.jpg");
                    }
                    $url        = _site_url("promotion/promotionDetail/". $row->id);
                ?>
                    <li>
                        <a href="<?php echo $url;?>"> 
                            <div class="thumbnail">
                                <img src="<?php echo $image;?>" alt="<?php echo @$row->alt;?>" />
                            </div>
                            <div class="caption">
                                <h4><?php echo @$row->name ? $row->name : "";?></h4>
                                <p><?php echo $url;?></p>
                                <p>
                                    <?php 
                                        echo utf8_substr(strip_tags($row->short_des), 0, 250);
                                    ?>
                                </p>
                            </div>
                        </a>
                        
                    </li>
                <?php
                }
            }
        }



        if( @$searchResult->data->result->activities->contents ){
            $len        = count($searchResult->data->result->activities->contents);
            if( $len > 0 ){
                foreach($searchResult->data->result->activities->contents  AS $row){
                    $image  = "";
                    if( $mobileDetect->isMobile() ){
                        $image = $row->thumb_mobile;
                    }else if( $mobileDetect->isTablet() ){
                        $image = $row->thumb_tablet;
                    }else{
                        $image = $row->thumb_desktop;
                    }
                    if( $image=="" ){
                        $image = base_url("public/image/thumb-no-img.jpg");
                    }
                    $url        = _site_url("promotion/activityDetail/". $row->id);
                ?>
                    <li>
                        <a href="<?php echo $url;?>">
                            <div class="thumbnail">
                                <img src="<?php echo $image;?>" alt="<?php echo @$row->alt;?>" />
                            </div>
                            <div class="caption">
                                <h4><?php echo @$row->name ? $row->name : "";?></h4>
                                <p><?php echo $url;?></p>
                                <p>
                                    <?php 
                                        echo utf8_substr(strip_tags($row->short_des), 0, 250);
                                    ?>
                                </p>
                            </div>
                        </a>
                        
                    </li>
                <?php
                }
            }
        }


        if( @$searchResult->data->result->lifestyle->contents ){
            $len        = count($searchResult->data->result->lifestyle->contents);
            if( $len > 0 ){
                foreach($searchResult->data->result->lifestyle->contents  AS $row){
                    $image  = "";
                    if( $mobileDetect->isMobile() ){
                        $image = $row->thumb_mobile;
                    }else if( $mobileDetect->isTablet() ){
                        $image = $row->thumb_tablet;
                    }else{
                        $image = $row->thumb_desktop;
                    }
                    if( $image=="" ){
                        $image = base_url("public/image/thumb-no-img.jpg");
                    }
                    $url        = _site_url("lifestyle/detail/". $row->id);
                ?>
                    <li>
                        <a href="<?php echo $url;?>">
                            <div class="thumbnail">
                                <img src="<?php echo $image;?>" alt="<?php echo @$row->alt;?>" />
                            </div>
                            <div class="caption">
                                <h4><?php echo @$row->name ? $row->name : "";?></h4>
                                <p><?php echo $url;?></p>
                                <p>
                                    <?php 
                                        echo utf8_substr(strip_tags($row->short_des), 0, 250);
                                    ?>
                                </p>
                            </div>
                        </a>
                        
                    </li>
                <?php
                }
            }
        }
    }
?>