<div class="mainContent">


    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->
    

    <div class="container">
    	
        <div class="row">
            <div class="col-md-12 col-xs-12">
            	<div class="search-results" data-sr>

                <div class="input-group">
                    <div class="input-group-btn search-panel">
                        <p>Search</p>
                    </div>
                    <form class="form-search-inpage" action="<?php echo _site_url("search")?>" method="get" onsubmit="return onSearchInPage();">
                        <input type="text" class="form-control" name="search" placeholder="คีย์เวริด์ที่ค้นหา" value="<?php echo @$_REQUEST['search'];?>">
                    </form>
                    <span class="input-group-btn">
                        <button class="btn" type="button" onclick="$('form.form-search-inpage').submit();"></button>
                    </span>
                </div>                
                    
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#viewAll" aria-controls="viewAll" role="tab" data-toggle="tab">View all</a></li>
                        <li role="presentation"><a href="#promotions" aria-controls="promotions" role="tab" data-toggle="tab">Promotions</a></li>
                        <li role="presentation"><a href="#activities" aria-controls="activities" role="tab" data-toggle="tab">Activities</a></li>
                        <li role="presentation"><a href="#lifestyle" aria-controls="lifestyle" role="tab" data-toggle="tab">Lifestyle</a></li>
                    </ul>
                    <div class="tab-content">
                        <?php
                            $limit          = 5;
                            $searchText     = @$_REQUEST['search'] ? $_REQUEST['search'] : "";
                            $searchResult   = getPurra()->search( $searchText );
                            $searchResult   = @json_decode($searchResult);
                            $searchText     = @$_REQUEST['search'] ? $_REQUEST['search'] : "";
                            $args           = array(
                                "searchResult"  => $searchResult, 
                                "limit"         => $limit,
                                "searchText"    => $searchText
                            );
                        ?>
                        <?php $this->load->view("search/tab_view_all",  array( "searchText" => $searchText ) );?>
                        <?php $this->load->view("search/tab_promotion", $args );?>
                        <?php $this->load->view("search/tab_activity",  $args );?>
                        <?php $this->load->view("search/tab_lifestyle", $args );?>
                    </div>
                    <!-- /.tab-content -->


                </div>
            </div>
        </div>
    
    	<div class="move_up"></div>
    </div><!-- /.container -->
    

</div><!-- /.mainContent -->