<?php
    if( @$rs ){
        $searchResult = $rs;


        if( @$searchResult->data->result->activities->contents ){
            $len        = count($searchResult->data->result->activities->contents);
            if( $len > 0 ){
                foreach($searchResult->data->result->activities->contents  AS $row){
                    $image  = "";
                    if( $mobileDetect->isMobile() ){
                        $image = $row->thumb_mobile;
                    }else if( $mobileDetect->isTablet() ){
                        $image = $row->thumb_tablet;
                    }else{
                        $image = $row->thumb_desktop;
                    }
                    if( $image=="" ){
                        $image = base_url("public/image/thumb-no-img.jpg");
                    }
                    $url        = _site_url("promotion/activityDetail/". $row->id);
                ?>
                    <li>
                        <div class="thumbnail">
                            <img src="<?php echo $image;?>" alt="<?php echo @$row->alt;?>" />
                        </div>
                        <div class="caption">
                            <h4><?php echo @$row->name ? $row->name : "";?></h4>
                            <p><a href="<?php echo $url;?>"><?php echo $url;?></a></p>
                            <p>
                                <?php 
                                    echo utf8_substr(strip_tags($row->short_des), 0, 250);
                                ?>
                            </p>
                        </div>
                    </li>
                <?php
                }
            }
        }


    }
?>