<div class="mainContent">

    <?php 
        $product        = getPurra()->product();
        $product        = @json_decode($product);
        $IS_MOBILE      = false;
        if( $mobileDetect->isMobile() || $mobileDetect->isTablet() ){
            $IS_MOBILE  = true;
        }
    ?>

    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->
    
    <style>
        <?php echo $product->data->product[0]->css;?>
    </style>
    
    <p>&nbsp;</p>
    <?php echo $product->data->product[0]->html;?>
    <p>&nbsp;</p>
    
    <?php /*
    <div class="container">
    

        <h1>น้ำแร่เพอร์ร่า<br> น้ำแร่ธรรมชาติ 100%</h1>
        
        
        <ol class="breadcrumb">
            <li><a href="<?php echo _site_url("home");?>">Home</a></li>
            <li><a>Product</a></li>
        </ol>
        

        
        <div class="row">
            <div class="col-md-12 col-xs-12">
            	
                <div data-sr="move -15px, over 2s">
                <div class="product-purra">
                    <img src="assets/images/product/product_purra.png" class="img-product-purra">
                    <ul class="pull-left">
                        <li>
                            <img src="assets/images/product/thumb-product/1.jpg" class="img-circle">
                            <p><span>บรรจุ ณ แหล่งกำเนิด</span>จากแหล่งน้ำใต้ดิน<br>ตามธรรมชาติ ลึกเกือบ 1,000 ฟุต<br>อายุกว่า 18,000 ปี</p>
                        </li>
                        <li>
                            <img src="assets/images/product/thumb-product/2.jpg" class="img-circle">
                            <p><span>เป็นน้ำแร่ธรรมชาติ 100%</span> ไม่มีการ<br>เติมแต่งแร่ธาตุใดๆ <span>“รสชาติไม่ฝาด<br>เฝื่อน เค็ม หรือขม”</span></p>
                        </li>
                        <li>
                            <img src="assets/images/product/thumb-product/3.jpg" class="img-circle">
                            <p><span>มีค่า pH เป็นกลาง ช่วยปรับสมดุล<br>ของร่างกาย </span>ทำให้เกิดภาวะสุขภาพ<br>ดีสูงสุด (Optimal Health)</p>
                        </li>
                    </ul>
                    <ul class="pull-right">
                        <li>
                            <img src="assets/images/product/thumb-product/4.jpg" class="img-circle">
                            <p><span>เปี่ยมไปด้วยแร่ธาตุ</span>ที่เป็นประโยชน์<br>ต่อร่างกายครบในขวดเดียว</p>
                        </li>
                        <li>
                            <img src="assets/images/product/thumb-product/5.jpg" class="img-circle">
                            <p><span>จัดเป็นน้ำแร่ชนิดเบา </span>จึงดื่มได้ทุกวัน <br>ไม่มีอันตรายต่อร่างกาย</p>
                        </li>
                        <li>
                            <img src="assets/images/product/thumb-product/6.jpg" class="img-circle">
                            <p><span>ช่วยชะล้างสารพิษในร่างกาย</span> หรือ<br>ที่เรารู้จักกันว่า <span>“ดีท็อกซ์ (DETOX)” </span></p>
                        </li>
                    </ul>
                </div>
                </div><!-- /data-sr -->
                
            </div>
        </div><!-- /.product-purra -->


        <div class="row">
            <div class="col-md-12 col-xs-12">
            	
                <div data-sr="move -15px, over 2s">
                <div class="purra">
                    <img src="assets/images/product/purra.png">
                    <div><span>1</span> น้ำแร่เพอร์ร่า เป็นน้ำแร่ที่ได้รับคัดเลือกในการใช้เสริฟ์บนสายการบินชั้นนำระดับโลกกว่า 30 สายการบิน ทำให้ได้มีโอกาสในการสร้างแบรนด์ให้เป็นที่รู้จักแก่นักท่องเที่ยวต่างชาติจำนวนมาก (ภายใต้การบริหารโดยบางกอกแอร์เวย์) </div>
                    <div><span>2</span> น้ำแร่ธรรมชาติเพอร์ร่า บรรจุจากแหล่งกำเนิดแหล่งพระงาม จ.สิงห์บุรี จากแหล่งน้ำใต้ดินตามธรรมชาติ ลึกเกือบ 1,000 ฟุต ซึ่งสะสมแร่ธาตุมายาวนาน เพราะครั้งหนึ่งเมื่อ 18,000 ปีที่แล้วเคยเป็นทะเลอันอุดมสมบูรณ์</div>
                    <div><span>3</span> เป็นน้ำแร่ธรรมชาติ 100% ไม่มีการเติมแต่งแร่ธาตุใดๆ “รสชาติไม่ฝาด เฝื่อน เค็ม หรือขม”</div>
                    <div><span>4</span> น้ำแร่ธรรมชาติเพอร์ร่า มีค่า pH เป็นกลาง ช่วยปรับสมดุลของร่างกาย ทำให้การทำงานของเซลล์เป็นปกติ สามารถดูดซึมอาหาร และแลกเปลี่ยนออกซิเจนได้ดีขึ้น ทำให้เกิด ภาวะสุขภาพดีสูงสุด (Optimal Health)</div>
                    <div><span>5</span> ช่วยชะล้างสารพิษในร่างกาย หรือที่เรารู้จักกันว่า “ดีท็อกซ์ (DETOX)” ควรดื่มน้ำแร่เพอร์ร่า 1 ขวดทันทีเมื่อตื่นนอน เพื่อชดเชยแร่ธาตุขับของเสียออกจากร่างกาย</div>
                    <div><span>6</span> เปี่ยมไปด้วยแร่ธาตุที่เป็นประโยชน์ต่อร่างกายครบในขวดเดียว จัดเป็นน้ำแร่ชนิดเบา จึงดื่มได้ทุกวัน ไม่มีอันตรายต่อร่างกาย</div>
                </div>
                </div><!-- /data-sr -->
            
            </div>
        </div><!-- /.purra -->

   	
        <div class="row">
            <div class="col-md-12 col-xs-12">
            	
                <div data-sr="move -15px, over 2s">
                <div class="all-pack">
                    <ul>
                        <li>
                            <div class="img-pack">
                                <div class="caption">
                                    <img src="assets/images/product/all-pack/330.png">
                                    <span>น้ำแร่เพอร์ร่า<br> 330 มล.</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img-pack">
                                <div class="caption">
                                    <img src="assets/images/product/all-pack/500.png">
                                    <span>น้ำแร่เพอร์ร่า<br> 500 มล.</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img-pack">
                                <div class="caption">
                                    <img src="assets/images/product/all-pack/750.png">
                                    <span>น้ำแร่เพอร์ร่า<br> 750 มล.</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="img-pack">
                                <div class="caption">
                                    <img src="assets/images/product/all-pack/1500.png">
                                    <span>น้ำแร่เพอร์ร่า<br> 1,500 มล.</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                </div><!-- /data-sr -->

            </div>
        </div><!-- /.all-pack -->
        
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <a href="http://www.singhaonlineshop.com/2015/products/19" target="_blank" >
                    <?php
                        $lang = getLang();
                        $imgIconPath = "assets/images/icons/icon-tab-shop-th-1.png";
                        switch( strtolower($lang) ){
                            case "th" : {
                                $imgIconPath = "assets/images/icons/icon-tab-shop-th-1.png";
                            } break;
                            
                            case "en" : 
                            default : {
                                $imgIconPath = "assets/images/icons/icon-tab-shop-en-1.png";
                            }
                        }
                    ?>
                    <img src="<?php echo $imgIconPath;?>"  class="img-block" />
                </a>
            </div>
        </div>
        <p />
        

        <div class="row">
            <div class="col-md-12 col-xs-12">
            	
                <div data-sr="move -15px, over 2s">
                <img src="assets/images/singha_online_shop.png" class="img-block">
                <div class="store">
                    สถานที่จัดจำหน่าย
                    <span>
                        <?php
                            if( $product ){
                                $len        = @$product->data->agent ? count($product->data->agent) : 0;
                                if( $len>0 ){
                                    for( $i=0;  $i<$len;  $i++ ){
                                        if( @$product->data->agent[$i] ){
                                            $item       = $product->data->agent[$i];
                                            $image      = base_url("public/image/thumb-no-img.jpg");
                                            if( $IS_MOBILE ){
                                                $image  = @$item->icon_mobile ? $item->icon_mobile : $image;
                                            }else{
                                                $image  = @$item->icon_desktop ? $item->icon_desktop : $image;
                                            }
                                        ?>
                                            <img src="<?php echo $image;?>" />
                                        <?php
                                        }
                                    }
                                }
                            }
                        ?>
                    </span>
                    และร้านค้าทั่วประเทศ
                </div>
                </div><!-- /data-sr -->
                
            </div>
        </div><!-- /.store -->
        
        
        <div class="move_up"></div>
    </div><!-- /.container -->
    */ ?>

<!--        <div class="row">
            <div class="col-md-12 col-xs-12">
                <a href="http://www.singhaonlineshop.com/2015/products/19" target="_blank" >
                    <?php
                        $lang = getLang();
                        $imgIconPath = "assets/images/icons/icon-tab-shop-th-1.png";
                        switch( strtolower($lang) ){
                            case "th" : {
                                $imgIconPath = "assets/images/icons/icon-tab-shop-th-1.png";
                            } break;
                            
                            case "en" : 
                            default : {
                                $imgIconPath = "assets/images/icons/icon-tab-shop-en-1.png";
                            }
                        }
                    ?>
                    <img src="<?php echo $imgIconPath;?>"  class="img-block" />
                </a>
            </div>
        </div>
        <p />-->
        

        <div class="row">
            <div class="col-md-12 col-xs-12">
            	
                <div data-sr="move -15px, over 2s">
                <img src="assets/images/singha_online_shop.png" class="img-block">
                <div class="store">
                    สถานที่จัดจำหน่าย
                    <span>
                        <?php
                            if( $product ){
                                $len        = @$product->data->agent ? count($product->data->agent) : 0;
                                if( $len>0 ){
                                    for( $i=0;  $i<$len;  $i++ ){
                                        if( @$product->data->agent[$i] ){
                                            $item       = $product->data->agent[$i];
                                            $image      = base_url("public/image/thumb-no-img.jpg");
                                            if( $IS_MOBILE ){
                                                $image  = @$item->icon_mobile ? $item->icon_mobile : $image;
                                            }else{
                                                $image  = @$item->icon_desktop ? $item->icon_desktop : $image;
                                            }
                                        ?>
                                            <img src="<?php echo $image;?>" />
                                        <?php
                                        }
                                    }
                                }
                            }
                        ?>
                    </span>
                    และร้านค้าทั่วประเทศ
                </div>
                </div><!-- /data-sr -->
                
            </div>
        </div><!-- /.store -->
        
        
        <div class="move_up"></div>
    
</div>