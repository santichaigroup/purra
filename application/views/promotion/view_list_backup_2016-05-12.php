<div class="mainContent">

    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->
    

    <div class="container">
    
    
        <div data-sr>
		<div class="row">
        	<div class="col-md-12 col-xs-12">
                <div class="half">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo _site_url("home");?>">Home</a></li>
                        <li><a href="<?php echo _site_url("promotion");?>">Promotions & Activities</a></li>
                        <li><a>All</a></li>
                    </ol>
                </div>
                <div class="half">
                    <div class="categories">
                    	<ul class="tabs">
                            <?php
                                $method = $this->uri->segment(3);
                            ?>
                            <li <?php echo $method==""||$method=="showlist"?' class="active" ':"";?> ><a href="<?php echo _site_url("promotion");?>">All</a></li>
                            <li <?php echo $method=="allPromotion"?' class="active" ':"";?> ><a href="<?php echo _site_url("promotion/allPromotion");?>">PROMOTIONS</a></li>
                            <li <?php echo $method=="allActivities"?' class="active" ':"";?> ><a href="<?php echo _site_url("promotion/allActivities");?>">ACTIVITIES</a></li>
                        </ul>
                    </div>
                </div>
            </div>
		</div>
        </div><!-- /data-sr -->
        
        
        <div class="content">
            <div class="row">
            
                <ul id="list-items" class="list-items">
                    <li>
                        <div class="thumbnail">
                            <a href="<?php echo _site_url("promotion/detail/1");?>"><img src="assets/images/promotion/thumb/1.jpg"></a>
                            <p>The Music Run 2015 ร่วมกิจกรรมลุ้นรับรางวัลสุดพิเศษ!  เพียง Capture หนุ่มสาวหน้าตาดีที่คุณรู้จักในคลิปวิดีโอ พร้อม Tag ชื่อคนๆ นั้น หรือ…</p>
                            <a href="<?php echo _site_url("promotion/detail/1");?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="<?php echo _site_url("promotion/detail/2");?>"><img src="assets/images/promotion/thumb/2.jpg"></a>
                            <p>Purra Music Run 2015 มาติดตามบรรยากาศความสนุกกับเสียงเพลงและกิจกรรม ภายในงาน The Music Run™ by AIA Vitality ไปพร้อมกันได้ที่นี่เลยค่ะ</p>
                            <a href="<?php echo _site_url("promotion/detail/2");?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="<?php echo _site_url("promotion/detail/3");?>"><img src="assets/images/promotion/thumb/3.jpg"></a>
                            <p>ทำนายดวงเมื่อไป The Music Run จากเดือนเกิดและเบอร์มือถือตัวสุดท้ายของคุณ</p>
                            <a href="<?php echo _site_url("promotion/detail/3");?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="<?php echo _site_url("promotion/detail/4");?>"><img src="assets/images/promotion/thumb/4.jpg"></a>
                            <p>ประกาศรายชื่อผู้โชคดีจากกิจกรรมลุ้นบัตร Music Run กับ Purra ผ่านทาง Instagram </p>
                            <a href="<?php echo _site_url("promotion/detail/4");?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="<?php echo _site_url("promotion/detail/5");?>"><img src="assets/images/promotion/thumb/5.jpg"></a>
                            <p>ตามหาผู้หญิงเบอร์ 07574 ผมยาว ผิวขาว สูงไม่เกิน 165 ใครรู้จักมีรางวัลนำจับให้ครับ</p>
                            <a href="<?php echo _site_url("promotion/detail/5");?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                </ul>
                
                <div class="col-xs-12 loadmore">
                    <div id="loadMore"></div>
                </div>
                
            </div>
        </div><!-- /#content -->
        
       <div class="move_up"></div>
        
    </div><!-- /.container -->
    

</div>