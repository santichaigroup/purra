<div class="mainContent">
    
    <?php $this->load->view("include/share_social");?>
    <!-- /#share -->
    

    <div class="container">
    
    
        <div data-sr>
		<div class="row">
        	<div class="col-md-12 col-xs-12">
                <div class="half">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo _site_url("home");?>">Home</a></li>
                        <li><a href="<?php echo _site_url("promotion");?>">Promotions & Activities</a></li>
                        <li><a>All</a></li>
                    </ol>
                </div>
                <div class="half">
                    <div class="categories">
                    	<ul class="tabs">
                            <?php
                                $method = $this->uri->segment(3);
                            ?>
                            <li <?php echo $method==""||$method=="showlist"?' class="active" ':"";?> ><a href="<?php echo _site_url("promotion");?>">All</a></li>
                            <li <?php echo $method=="allPromotion"?' class="active" ':"";?> ><a href="<?php echo _site_url("promotion/allPromotion");?>">PROMOTIONS</a></li>
                            <li <?php echo $method=="allActivities"?' class="active" ':"";?> ><a href="<?php echo _site_url("promotion/allActivities");?>">ACTIVITIES</a></li>
                        </ul>
                    </div>
                </div>
            </div>
		</div>
        </div><!-- /data-sr -->
        
        
        <div class="content">
            <div class="row">
            
                <ul id="list-items" class="list-items">
                    <?php
                        $limit  = 10;
                        $rs     = getPurra()->promotionActivity( $limit );
                        $rs     = @json_decode($rs);
                        $len    = 0;
                        $total  = 0;
                        if( $rs  &&  $rs->code==200 ){
                            $len        = is_array($rs->data->content) ? count($rs->data->content) : 0;
                            $total      = $rs->data->quantity;
                            for( $i=0;  $i<$len;  $i++ ){
                                $item   = $rs->data->content[$i];
                                $url    = _site_url("promotion/".($item->type=="promotion"?"promotionDetail":"activityDetail")."/{$item->id}");
                                $image  = "";
                                if( $mobileDetect->isMobile() ){
                                    $image = $item->thumb_mobile;
                                }else if( $mobileDetect->isTablet() ){
                                    $image = $item->thumb_tablet;
                                }else{
                                    $image = $item->thumb_desktop;
                                }
                                if( $image=="" ){
                                    $image = base_url("public/image/thumb-no-img.jpg");
                                }
                            ?>
                                <li>
                                    <div class="thumbnail">
                                        <a href="<?php echo $url;?>">
                                            <img src="<?php echo $image;?>" alt="<?php echo $item->alt; ?>" />
                                        </a>
                                        <p><?php echo $item->name;?></p>
                                        <a href="<?php echo $url;?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                                    </div>
                                </li>
                            <?php
                            }
                        }
                    ?>
                </ul>
                
                <?php
                    if( $len>0  &&  $len>=$limit  &&  $len<$total ){
                    ?>
                        <div class="col-xs-12 loadmore">
                            <div id="loadMore"></div>
                        </div>
                    <?php
                    }
                ?>
                
            </div>
        </div><!-- /#content -->
        
       <?php
            if( $len>0 ){
            ?>
                <div class="move_up"></div>
            <?php
            }
        ?>
        
    </div><!-- /.container -->
    

</div>

<script>
    $(function(){
        $('#loadMore').click(function(){
            $.post( "<?php echo _site_url("promotion/loadmorePromotionActivity")?>",{
                        page : Math.ceil($('ul#list-items > li').toArray().length/<?php echo $limit;?>)+1,
                        limit : '<?php echo $limit;?>'
                    }, function(html){
                        if( html=="" ){
                            $('#loadMore').hide();
                        }else{
                            $('ul#list-items').append(html);
                            if($('ul#list-items > li').toArray().length >= <?php echo $total;?>){
                                $('#loadMore').fadeOut();
                            }else 
                                if( Math.ceil($('ul#list-items > li').toArray().length % <?php echo $limit;?>) !=0 ){
                                $('#loadMore').fadeOut();
                            }
                        }
                    });
        });
    });
</script>