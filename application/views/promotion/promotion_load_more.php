<?php
if( $rs  &&  $rs->code==200 ){
    $len        = is_array($rs->data->promotion) ? count($rs->data->promotion) : 0;
    for( $i=0;  $i<$len;  $i++ ){
        $item   = $rs->data->promotion[$i];
        $url    = _site_url("promotion/promotionDetail/{$item->id}");
        $image  = "";
        if( $mobileDetect->isMobile() || $mobileDetect->isTablet() ){
            $image = @$item->thumb_mobile ? $item->thumb_mobile : "";
        }else{
            $image = @$item->thumb_desktop ? $item->thumb_desktop : "";
        }
        if( $image=="" ){
            $image = base_url("public/image/thumb-no-img.jpg");
        }
    ?>
        <li>
            <div class="thumbnail">
                <a href="<?php echo $url;?>">
                    <img src="<?php echo $image;?>" alt="<?php echo $item->alt; ?>" />
                </a>
                <p><?php echo $item->name;?></p>
                <a href="<?php echo $url;?>" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
            </div>
        </li>
    <?php
    }
}