<div class="mainContent">


    <div class="share">
        <ul class="share-social">
        	<li><a href="#"></a></li>
        	<li><a href="#"></a></li>
        	<li><a href="#"></a></li>
        	<li><a href="#"></a></li>
        </ul>
    </div>
    <!-- /#share -->
    

    <div class="container">
    
		
        <div data-sr>
		<div class="row">
        	<div class="col-md-12 col-xs-12">
                <div class="half">
                    <ol class="breadcrumb">
                        <li><a href="home.html">Home</a></li>
                        <li><a href="promotion.html">Promotions & Activities</a></li>
                        <li><a>All</a></li>
                    </ol>
                </div>
                <div class="half">
                    <div class="categories">
                    	<ul class="tabs">
                        	<li class="active"><a href="#">All</a></li>
                        	<li><a href="#">PROMOTIONS</a></li>
                        	<li><a href="#">ACTIVITIES</a></li>
                        </ul>
                    </div>
                </div>
            </div>
		</div>
        </div><!-- /data-sr -->

		
    	<div class="content">
            <div class="row">
            
                <ul id="list-items" class="list-items">
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/1.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/2.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/3.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/4.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/5.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/6.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/7.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/8.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/9.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/10.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/1.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/2.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/3.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="thumbnail">
                            <a href="promotion-detail.html"><img src="assets/images/promotion/thumb/4.jpg"></a>
                            <p>มาร่วมวิ่งบนเส้นทางแห่งเสียงเพลงกับแนวดนตรีสุดฮิพหลากหลายในงาน“เดอะมิวสิค รัน บาย เอไอเอ</p>
                            <a href="promotion-detail.html" class="read-more">อ่านรายละเอียด<span class="glyphicon glyphicon-triangle-right"></span></a>
                        </div>
                    </li>
                </ul>
                
                <div class="col-xs-12 loadmore">
                    <div id="loadMore"></div>
                </div>
                
            </div>
        </div><!-- /#content -->
        
    
        <div class="move_up"></div>
    </div><!-- /.container -->
    

</div>