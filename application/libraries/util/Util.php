<?php
    class Util{        
        public function compareDate($date,$age){
            $age = intval($age);
            $age = intval($age*86400);
            $datenow = strtotime("now");
            $datepost = strtotime($date);
            $compair = intval($datenow - $datepost);
            $result = ($compair < $age || $datenow<$datepost )?true:false;
            return $result;
        }
        public function getNewImage($create_date, $comparedDay=15){
            if($this->compareDate($create_date, $comparedDay)){
                return '<img src="'.Site::$fullURL.'images/new.gif" />';
            }
        }
        
        
        public function eventDisplayDate($startDate,$endDate)
	{
		$m = array( "",
                            "มกราคม",   "กุมภาพันธ์",  "มีนาคม",    "เมษายน", 
                            "พฤษภาคม",  "มิถุนายน",   "กรกฎาคม",  "สิงหาคม", 
                            "กันยายน",   "ตุลาคม",    "พฤศจิกายน", "ธันวาคม");
		$startDateArr=explode('-',$startDate,3);
		$endDateArr=explode('-',$endDate,3);
		$startmonth = $m[intval($startDateArr[1])];
		$endmonth = $m[intval($endDateArr[1])];
		$startyear = intval($startDateArr[0]+543);
		$endyear = intval($endDateArr[0]+543);
		//$startyear = substr($startyear,2,2);
		//$endyear = substr($endyear,2,2);
		$startDateArr[2] = intval($startDateArr[2]);
		$endDateArr[2] = intval($endDateArr[2]);
		if($startDateArr[0]<>$endDateArr[0]){
			return "{$startDateArr[2]} {$startmonth} {$startyear} - {$endDateArr[2]} {$endmonth} {$endyear}";
		}
		if($startDateArr[1]<>$endDateArr[1]){
			return "{$startDateArr[2]} {$startmonth} - {$endDateArr[2]} {$endmonth} {$endyear}";
		}
		if($startDateArr[2]<>$endDateArr[2]){
			return "{$startDateArr[2]} - {$endDateArr[2]} {$endmonth} {$endyear}";
		}
		if($startDateArr[2]==$endDateArr[2]){
			return "{$startDateArr[2]} {$startmonth} {$startyear}";
		}
		return "Invalid date input";
	}
	
	
	public function eventDisplayDate_eng($startDate,$endDate)
	{
		$m = array("","January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		$startDateArr=explode('-',$startDate,3);
		$endDateArr=explode('-',$endDate,3);
		$startmonth = $m[intval($startDateArr[1])];
		$endmonth = $m[intval($endDateArr[1])];
		$startyear = intval($startDateArr[0]);
		$endyear = intval($endDateArr[0]);
		//$startyear = substr($startyear,2,2);
		//$endyear = substr($endyear,2,2);
		$startDateArr[2] = intval($startDateArr[2]);
		$endDateArr[2] = intval($endDateArr[2]);
		
		if($startDateArr[0]<>$endDateArr[0]){
			return "{$startmonth} {$startDateArr[2]}, {$startyear} - {$endmonth} {$endDateArr[2]}, {$endyear}";
		}
		if($startDateArr[1]<>$endDateArr[1]){
			return "{$startmonth} {$startDateArr[2]} - {$endmonth} {$endDateArr[2]}, {$endyear}";
		}
		if($startDateArr[2]<>$endDateArr[2]){
			return "{$endmonth} {$startDateArr[2]} - {$endDateArr[2]}, {$endyear}";
		}
		if($startDateArr[2]==$endDateArr[2]){
			return "{$startmonth} {$startDateArr[2]}, {$startyear}";
		}
		return "Invalid date input";
	}
        
        
        
        
    }
    
    
    /**
     * @return Util 
     */
    function getUtilObject(){
        if(!isset($_ENV['system-util'])){
            $obj                    = new Util();
            $_ENV['system-util']    = &$obj;
        }
        return $_ENV['system-util'];
    }
    
    
    /**
     *
     * @param DATETIME $date
     * @param Int $age
     * @return String 
     */
    function compareDate($date,$age){
        $obj                    = getUtilObject();
        return $obj->compareDate($date,$age);
    }
    
    
    /**
     *
     * @param DATETIME $create_date
     * @param int $comparedDay
     * @return String HTML
     * @example <img src="news.gif" /> 
     */
    function getNewImage($create_date, $comparedDay=15){
        $obj                    = getUtilObject();
        return $obj->getNewImage($create_date, $comparedDay);
    }
    
    
    
    function eventDisplayDate($startDate,$endDate){
        $obj                    = getUtilObject();
        return $obj->eventDisplayDate($startDate,$endDate);
    }
    
    
    
    function eventDisplayDate_eng($startDate,$endDate){
        $obj                    = getUtilObject();
        return $obj->eventDisplayDate_eng($startDate,$endDate);
    }
    
    
    
    function utf8_substr($str,$start_p,$len_p) { 
        if(strlen($str) < $len_p){
                return $str;	
        }
        preg_match_all("/./u", $str, $ar); 
        if(func_num_args() >= 3) { 
            $end = func_get_arg(2); 
            return join("",array_slice($ar[0],$start_p,$len_p)) . "..."; 
        } else { 
            return join("",array_slice($ar[0],$start_p)) . "..."; 
        } 
    }
    
    
    
    function substr_utf8( $str, $start_p , $len_p){
        $ret =  preg_replace(   '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$start_p.'}'.
                                '((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len_p.'}).*#s',
                                '$1' , 
                                $str );
        if(strlen($str) > $len_p){
                $ret .= "...";
        }
        return $ret;
    };
    
    
    
    function getQueryString(){
        return $_SERVER['QUERY_STRING'];
    }
    
    
    
    /**
     *
     * @param String $haystack
     * @param Array $replacementArray 
     * @return String
     */
    function getPrepareText( $haystack, $replacementArray=Array() ){
        $len                = count($replacementArray);
        $key                = array_keys($replacementArray);
        for( $i=0;  $i<$len;  $i++ ){
            $haystack       = str_replace("@{$key[$i]}", $replacementArray[$key[$i]], $haystack);
        }
        return $haystack;
    }
    
    
    function is_iOS(){
        $userAgent                          = strtolower($_SERVER['HTTP_USER_AGENT']);
        if(strstr($userAgent, 'iphone') || strstr($userAgent, 'ipad') || strstr($userAgent, 'ipod') ){
            return true;
        }
        return false;
    }

	
    function showCategoryName(&$id){
        $id					= explode("-", $id);
        $tableName				= $id[1];
        $id                                 = $id[0];
        $db					= getDBO();
        $db->setQuery(" SELECT cate_name FROM {$tableName} WHERE id='{$id}' ");
        $rs					= $db->loadAssocList();
        $id					= @$rs[0]['cate_name']?$rs[0]['cate_name']:'-';
    }

    
    function countContentViaCategory($cateId, $tblName){
            $db													= getDBO();
            $db->setQuery(" SELECT count(id) AS total FROM {$tblName} WHERE status='1' AND cid='{$cateId}'  ");
            $rs													= $db->loadAssocList();
            return $rs[0]['total'];
    }


    function getAllCategory( $currentId, $tableName, $currentParentId=-1, $cateId=-1){        
        $db					= getDBO();
        $db->setQuery("SELECT id FROM  {$tableName} WHERE status='1' AND parent_id='{$currentId}' ORDER BY id DESC ");
        $rs					= $db->loadAssocList();
        $len					= count($rs);
        if(count($len)){
            $output				= Array();
            $store				= Array();
            for( $i=0;  $i<$len;  $i++ ){
                    $store[]			= Array(
                            'id'	=> $rs[$i]['id'],
                            'level'	=> 1
                    );
            }
            while( count($store)>0 ){
                    $buf				= array_pop($store);
                    $output[]		= $buf;
                    $db->setQuery("SELECT id FROM  {$tableName} WHERE status='1' AND parent_id='{$buf['id']}'   ORDER BY id DESC ");
                    $rs					= $db->loadAssocList();
                    $len					= count($rs);
                    for( $i=0;   $i<$len;  $i++ ){
                            $row			= Array(
                                    'id'	=> $rs[$i]['id'],
                                    'level'	=> ($buf['level']+1)
                            );
                            array_push($store, $row );
                    }
            }
            $html				= "";
            $len				= count($output);
            $html				.='
                    <select name="parent_id" id="parent_id" style="min-width:300px;" class="required">
                            <option  value="">'.lang('select category', 'select category').'</option>
            ';
            for(  $i=0;   $i<$len;   $i++  ){
                $db->setQuery("SELECT id, cate_name FROM {$tableName}  WHERE id='{$output[$i]['id']}' ");
                $rs				= $db->loadAssocList();
                $rs				= $rs[0];
                $tab				= '';
                for($loop=0;$loop<($output[$i]['level']-1);$loop++){$tab.='&mdash;';}
                $selected			= $rs['id']==$currentParentId ? ' selected="selected" ' : '' ;
                $disabled			= $rs['id']==$cateId ? ' disabled="disabled" ' : '' ;
                if($rs['cate_name']=='main'){
                    $rs['cate_name']            = 'หมวดหลัก';
                }
                $html					.='
                        <option value="'.$rs['id'].'" '.$selected.' '.$disabled.'  >&nbsp;'.$tab.' '.$rs['cate_name'].'</option>
                ';		
            }
            return $html;
        }else{
            return "";
        }
    }


    function getAllCID( $currentId, $tableName, $cateId=-1){        
        return str_replace('parent_id', 'cid', getAllCategory($currentId, $tableName, $cateId));
    }


    /* Webboard  */
    function getSubDetail(&$detail){
        $detail                 = utf8_substr(strip_tags($detail), 0, 60);
    }

    
    function getMonthName(&$index, $lang='th'){
        $month 			= array(    0	=>	array(	"th"	=> 	"มกราคม",
                                                                "en"	=> 	"January"),
                                            1	=>	array(	"th"    =>	"กุมภาพันธ์"	,
                                                                "en"	=>	"February"),
                                            2	=>	array(	"th"	=>	"มีนาคม",
                                                                "en"	=>	"March"),
                                            3	=>	array(	"th"	=>	"เมษายน",
                                                                "en"	=>	"April"),
                                            4	=>	array(	"th"	=>	"พฤษภาคม",
                                                                "en"	=>	"May"),
                                            5	=>	array(	"th"	=>	"มิถุนายน",
                                                                "en"	=>	"June"),
                                            6	=>	array(	"th"	=>	"กรกฎาคม",
                                                                "en"	=>	"July"),
                                            7	=>	array(	"th"	=>	"สิงหาคม",
                                                                "en"	=>	"August"),
                                            8	=>	array(	"th"	=>	"กันยายน",
                                                                "en"	=>	"September"),
                                            9	=>	array(	"th"	=>	"ตุลาคม",
                                                                "en"	=>	"October"),
                                            10	=>	array(	"th"	=>	"พฤศจิกายน",
                                                                "en"	=>	"November"),
                                            11	=>	array(	"th"	=>	"ธันวาคม",
                                                                "en"	=>	"December")
                                            );
        $index =   $month[$index-1][strtolower($lang)];
    }

    
    function getRecommend(&$val){
        if($val==1){
            $val        = '<span style="color:blue">ข่าวเด่น</span>';
        }else{
            $val        = '-';
        }
    }
    
    
    
    function dumpAll($value='', $file='output.txt'){
        $f              = fopen($file, 'w');
        ob_start();
        echo "VALUE";
        echo "\n";
        var_dump($value);
        echo "REQUEST";
        echo "\n";
        var_dump($_REQUEST);
        echo "FILES";
        echo "\n";
        var_dump($_FILES);
        echo "SERVER";
        echo "\n";
        var_dump($_SERVER);
        $output         = ob_get_clean();
        fwrite($f, $output);
        fclose($f);
    }
    
    
    function setCurrentAdminTab( $index ){
        $_ENV['alias-tab-number']           = $index;
    }
    
    
    function getTemplate_jquery(){
        $db                 = getdbo();
        $db->setQuery(" SELECT      jquery 
                        FROM        cpanel_template
                        WHERE       local_id='".Site::$local_id."'
                        ");
        $rs                 = $db->loadAssocList();
        return @$rs[0]['jquery']?$rs[0]['jquery'] : 'blitzer' ;
    }
    
    
    function isShowMenuIcon(){
        return @$_ENV['user-config']['showMenuIcon'] ? $_ENV['user-config']['showMenuIcon'] : false;
    }
    
    
    function pre($arr=Array()){  echo "<pre>"; var_dump($arr); echo "</pre>"; }
    
    
    function getStatus($status=0){
        switch( $status ){
            case 0      :
                return '<span class="label label-warning">Draft</span>';
                break;
            case 1      :
                return '<span class="label label-success">Publish</span>';
                break;
            case 2      : 
                return '<span class="label label-danger">Deleted</span>';
                break;
        }
        return '';
    }
    
    
    function langs($key){
        if(strtolower(getLang())=='en' ){
            return $key;
        }
        $CI         = &get_instance();
        $v          = $CI->lang->line($key);
        return $v?$v:$key;
    }
    
    function eventDisplayDate_short($startDate,$endDate)
    {
        $m = array( "",
                    "ม.ค.",   "ก.พ.",  "มี.ค.",    "เม.ย.", 
                    "พ.ค.",  "มิ.ย.",   "ก.ค.",  "ส.ค.", 
                    "ก.ย.",   "ต.ค.",    "พ.ย.", "ธ.ค.");
        $startDateArr=explode('-',$startDate,3);
        $endDateArr=explode('-',$endDate,3);
        $startmonth = $m[intval($startDateArr[1])];
        $endmonth = $m[intval($endDateArr[1])];
        $startyear = intval($startDateArr[0]+543);
        $endyear = intval($endDateArr[0]+543);
        //$startyear = substr($startyear,2,2);
        //$endyear = substr($endyear,2,2);
        $startDateArr[2] = intval($startDateArr[2]);
        $endDateArr[2] = intval($endDateArr[2]);
        if($startDateArr[0]<>$endDateArr[0]){
                return "{$startDateArr[2]} {$startmonth} {$startyear} - {$endDateArr[2]} {$endmonth} {$endyear}";
        }
        if($startDateArr[1]<>$endDateArr[1]){
                return "{$startDateArr[2]} {$startmonth} - {$endDateArr[2]} {$endmonth} {$endyear}";
        }
        if($startDateArr[2]<>$endDateArr[2]){
                return "{$startDateArr[2]} - {$endDateArr[2]} {$endmonth} {$endyear}";
        }
        if($startDateArr[2]==$endDateArr[2]){
                return "{$startDateArr[2]} {$startmonth} {$startyear}";
        }
        return "Invalid date input";
    }
    
    
    
    function getLang(){
//        $ci         = &get_instance();
//        return $ci->session->userdata('lang');
        if( @isset($_COOKIE["language"]) ){
            return $_COOKIE["language"];
        }else{
            return 'th';
        }
    }
    
    
    function setLang($lang = 'th'){
        setcookie("language", $lang, time() + (86400 * 30), "/");
    }
    
    
    function _site_url($uri){
        return site_url(getLang().'/'.$uri);
    }
    
    
?>