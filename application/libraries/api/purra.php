<?php
class Purra{
    
    // DEMO
//    private $prefixUrl = "http://www.singha.com/demo-brandsite/";
//    private $secreteKey = "J0C3ssH4x2jOwHHHhkYyhDb4/x9I9BalqwkH4W3Lf6lepBB+Xu/lFtlHyZijinPa5/x2BPO+zX3b6aQ4k6Z9ag==";
    
    
    // DEV
    //const prefixUrl = "http://www.singha.com/dev-brandsite/";
    private $prefixUrl = "http://www.singha.com/dev-brandsite/";
    private $secreteKey = "ytv3zyEXJTGmNqrNUBQM5wnZNZY3XLu/hG+u/0279C8QmlG7sT7n6gx8+ChfK+9WFAbaMA6RJHptcKCQuktO/A==";
    
    private $DATA       = array();
    
    
    
    public function __construct() {
        $this->DATA['key'] = $this->secreteKey;
    }

    /**
     * 
     * @param number $type
     * @param String $subject
     * @param String $name
     * @param String $email
     * @param String $phone
     * @param String $msg
     * @param String $language
     */
    public function contact( $type, $subject, $name, $email, $phone, $msg ){
        $url = $this->prefixUrl. "api/contact/";
        $this->DATA['type']     = $type;
        $this->DATA['subject']  = $subject;
        $this->DATA['name']     = $name;
        $this->DATA['email']    = $email;
        $this->DATA['phone']    = $phone;
        $this->DATA['msg']      = $msg;
        $this->DATA['language'] = getLang();
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function header(){
        $url = $this->prefixUrl. "api/header/";
        $this->DATA['language'] = getLang();
        
        if(!isset($_ENV['api-purra-header'])){
            $output = $this->post($url, $this->DATA);
            $_ENV['api-purra-header']    = &$output;
        }
        return $_ENV['api-purra-header'];
    }
    
    
    public function footer(){
        $url = $this->prefixUrl. "api/footer/";
        $this->DATA['language'] = getLang();
        
        if(!isset($_ENV['api-purra-footer'])){
            $output = $this->post($url, $this->DATA);
            $_ENV['api-purra-footer']    = &$output;
        }
        return $_ENV['api-purra-footer'];
    }
    
    
    public function social($jsonDecode = false){
        $url = $this->prefixUrl. "api/social/";
        $this->DATA['language'] = getLang();
        
        if(!isset($_ENV['api-purra-social'])){
            $output = $this->post($url, $this->DATA);
            $_ENV['api-purra-social']    = &$output;
        }
        if( $jsonDecode ){
            return json_decode($_ENV['api-purra-social']);
        }
        return $_ENV['api-purra-social'];
    }
    
    
    
    public function newsLetter($email){
        $url = $this->prefixUrl. "api/newsletter/";
        $this->DATA['email']    = $email;
        $this->DATA['language'] = getLang();
        return $this->post($url, $this->DATA);
    }
    
    
    public function instagram(){
        $url = $this->prefixUrl. "api/instagram/";
        $this->DATA['language'] = getLang();
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function highlight($perpage=20, $page=1){
        $url = $this->prefixUrl. "api/highlight/";
        $this->DATA['language'] = getLang();
        $this->DATA['perpage'] = $perpage;
        $this->DATA['page'] = $page;
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function promotionBanner(){
        $url = $this->prefixUrl. "api/promotion_banner/";
        $this->DATA['language'] = getLang();
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function promotion($perpage=20, $page=1){
        $url = $this->prefixUrl. "api/promotion/";
        $this->DATA['language'] = getLang();
        $this->DATA['perpage'] = $perpage;
        $this->DATA['page'] = $page;
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function promotionDetail($id){
        $url = $this->prefixUrl. "api/promotion/";
        $this->DATA['language'] = getLang();
        $this->DATA['id'] = $id;
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function activity($perpage=20, $page=1){
        $url = $this->prefixUrl. "api/activity/";
        $this->DATA['language'] = getLang();
        $this->DATA['perpage'] = $perpage;
        $this->DATA['page'] = $page;
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function activityDetail($id){
        $url = $this->prefixUrl. "api/activity/";
        $this->DATA['language'] = getLang();
        $this->DATA['id'] = $id;
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function activityBanner(){
        $url = $this->prefixUrl. "api/activity_banner/";
        $this->DATA['language'] = getLang();
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function promotionActivity($perpage=20, $page=1){
        $url = $this->prefixUrl. "api/promotionactivity/";
        $this->DATA['language'] = getLang();
        $this->DATA['perpage'] = $perpage;
        $this->DATA['page'] = $page;
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function promotionActivityDetail($id){
        $url = $this->prefixUrl. "api/promotionactivity/";
        $this->DATA['language'] = getLang();
        $this->DATA['id'] = $id;
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    
    public function lifestyleCategory(){
        $url = $this->prefixUrl. "api/lifestyle_category/";
        $this->DATA['language'] = getLang();
        $output = $this->post($url, $this->DATA);
        return $output;
    } 
    
    
    public function lifestyle($category=0, $perpage=20, $page=1){
        $url = $this->prefixUrl. "api/lifestyle/";
        
        $this->DATA['language'] = getLang();
        if( $category ){
            $this->DATA['category'] = $category;
        }
        
        $this->DATA['perpage'] = $perpage;
        $this->DATA['page'] = $page;
        $output = $this->post($url, $this->DATA);
        
        return $output;
    }
    
    
    public function lifestyleDetail($id){
        $url = $this->prefixUrl. "api/lifestyle/";
        $this->DATA['language'] = getLang();
        $this->DATA['id'] = $id;
        $output = $this->post($url, $this->DATA);
        
        return $output;
    }
    
    
    public function product(){
        $url = $this->prefixUrl. "api/product/";
        $this->DATA['language'] = getLang();
        //$this->DATA['perpage'] = $perpage;
        //$this->DATA['page'] = $page;
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    public function getContact(){
        if(!isset($_ENV['api-purra-get-contact'])){
            $url = $this->prefixUrl. "api/get_contact/";
            $this->DATA['language'] = getLang();
            $output = $this->post($url, $this->DATA);
            $_ENV['api-purra-get-contact']    = &$output;
        }
        return $_ENV['api-purra-get-contact'];
    }
    
    
    
    public function site_background(){
        $key = "api-app-site-background";
        if(!isset($_ENV[$key])){
            $url = $this->prefixUrl. "api/site-background/";
            $this->DATA['language'] = getLang();
            $output = $this->post($url, $this->DATA);
            $_ENV[$key]    = &$output;
        }
        return $_ENV[$key];
    }
    
    
    
    public function seo(){
        $key = "api-app-seo";
        if(!isset($_ENV[$key])){
            $url = $this->prefixUrl. "api/seo/";
            $this->DATA['language'] = getLang();
            $output = $this->post($url, $this->DATA);
            $_ENV[$key]    = &$output;
        }
        return $_ENV[$key];
    }
    
    
    public function youtube(){
        $key = "api-app-youtube";
        if(!isset($_ENV[$key])){
            $url = $this->prefixUrl. "api/youtube/";
            $this->DATA['language'] = getLang();
            $output = $this->post($url, $this->DATA);
            $_ENV[$key]    = &$output;
        }
        return $_ENV[$key];
    }
    
    public function product_home(){
        $key = "api-app-product-home";
        if(!isset($_ENV[$key])){
            $url = $this->prefixUrl. "api/product-home/";
            $this->DATA['language'] = getLang();
            $output = $this->post($url, $this->DATA);
            $_ENV[$key]    = &$output;
        }
        return $_ENV[$key];
    }
    
    public function specialEvent(){
        $key = "api-app-special-event";
        if(!isset($_ENV[$key])){
            $url = $this->prefixUrl. "api/special-event/";
            $this->DATA['language'] = getLang();
            $output = $this->post($url, $this->DATA);
            $_ENV[$key]    = &$output;
        }
        return $_ENV[$key];
    }
    
    
    
    public function search($query, $perpage=5, $page=1){
        $url = $this->prefixUrl. "api/search/";
        $this->DATA['language'] = getLang();
        $this->DATA['perpage'] = $perpage;
        $this->DATA['page'] = $page;
        $this->DATA['query'] = $query;
        $output = $this->post($url, $this->DATA);
        return $output;
    }
    
    
    
    public function post($url, $data=array()){
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context  = stream_context_create($options);
        $result = @file_get_contents($url, false, $context);
        
        $this->DATA['id'] = "";
        $this->DATA['perpage'] = '';
        $this->DATA['page'] = '';
        
        return $result;
    }
    
    
    public function getPrefixUrl(){
        return $this->prefixUrl;
    }
    
    
    
    
}


/**
 * 
 * @return \Purra
 */
function getPurra(){
    if(!isset($_ENV['api-purra'])){
        $obj                    = new Purra();
        $_ENV['api-purra']    = &$obj;
    }
    return $_ENV['api-purra'];
}