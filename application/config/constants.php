<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


define("reCAPTCHA_KEY",         "6Le5ghMTAAAAAKwCbWFH8UwL4NkEdnGlc6-eiVbZ");
define("reCAPTCHA_SECRET_KEY",  "6Le5ghMTAAAAAGZCBBB2k9E8OcxiGcWnmmzm8G7u");

/* Production */
define("FB_APP_ID",         "472207782965312");
define("FB_APP_SECRET",     "6ebf49d95f0f4d68d36af87e4e3b4651");
/* Development */
//define("FB_APP_ID",         "520330611486362");
//define("FB_APP_SECRET",     "19b001dbeac7109db5726f26baf88c55");

/* End of file constants.php */
/* Location: ./application/config/constants.php */