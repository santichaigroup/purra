<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {
    
    public $cmd = "contact";
    private $data = array();
    
    
    public function __construct() {
        parent::__construct();
        
        $this->data['body_class'] = "contact";
        $this->data['mobileDetect'] = new Mobile_Detect();
    }
    
    
    public function index(){
        
        $content        = getPurra()->getContact();
        $content        = @json_decode($content);
        
        $this->data['_TITLE']   = @$content->data->seo->title       ? $content->data->seo->title : "";
        $this->data['_DESC']    = @$content->data->seo->description ? $content->data->seo->description : "";
        $this->data['_KEYWORD'] = @$content->data->seo->keyword     ? $content->data->seo->keyword : "";
        
        $this->data['_BODY'] = $this->getView("{$this->cmd}/view_main");
        $this->load->view('template', $this->data);
    }
    
    
    private function getView($name){
        ob_start();
        $this->load->view($name, $this->data);
        return ob_get_clean();
    }
    
}