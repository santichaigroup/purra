<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class IntroTest extends CI_Controller {
    
    
    public function __construct() {
        parent::__construct();
    
        $this->data['mobileDetect'] = new Mobile_Detect();
    }
    
    public function index(){
        $this->load->view('view_intro_test');
    }
    
}