<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {
    
    public $cmd = "contact";
    private $data = array();
    
    
    
    
    
    public function __construct() {
        parent::__construct();
        $this->data['mobileDetect'] = new Mobile_Detect();
    }
    
    public function header(){
        echo getPurra()->header();
    }
    
    public function footer(){
        echo getPurra()->footer();
    }
    
    public function instagram(){
        echo getPurra()->instagram();
    }
    
    public function highlight(){
        echo getPurra()->highlight();
    }
    
    public function promotion(){
        echo getPurra()->promotion();
    }
    
    
    public function promotionActivity(){
        echo getPurra()->promotionActivity();
    }
    
    
    public function lifestyleCategory(){
        echo getPurra()->lifestyleCategory();
    }
    
    public function lifestyle(){
        echo getPurra()->lifestyle(12);
    }
    
    public function lifestyleDetail(){
        echo getPurra()->lifestyleDetail(24);
    }
    
    
    public function product(){
        echo getPurra()->product();
    }
    
    public function getContact(){
        echo getPurra()->getContact();
    }
    
    public function social(){
        echo getPurra()->social();
    }
    
    
    public function postForm(){
        pre($_POST);
    }
    
}