<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {
    
    public $cmd = "search";
    private $data = array();
    
    
    public function __construct() {
        parent::__construct();
        $this->data['body_class'] = "search";
        $this->data['mobileDetect'] = new Mobile_Detect();
    }
    
    
    public function index(){
        $this->showlist();
    }
    
    
    public function showlist(){
        $view = "{$this->cmd}/view_list";
        $this->data['_BODY']    = $this->getView($view);
        $this->load->view('template', $this->data);
    }
    
    
    /*
    public function loadmore(){
        $page       = $this->input->post('page');
        $limit      = $this->input->post('limit');
        
        $rs         = getPurra()->promotion($limit, $page);
        $rs         = @json_decode($rs);
        $this->data['rs'] = $rs;
        echo $this->getView("promotion/promotion_load_more");
    }
     * 
     */
    
    
    public function loadmore_tabAll(){
        $page       = $this->input->post('page');
        $limit      = $this->input->post('limit');
        $query      = $this->input->post('query');
        
        $rs         = getPurra()->search($query, $limit, $page);
        $rs         = @json_decode($rs);
        $this->data['rs'] = $rs;
        echo $this->getView("{$this->cmd}/loadmore_tab_all");
    }
    
    public function loadmore_tabPromotion(){
        $page       = $this->input->post('page');
        $limit      = $this->input->post('limit');
        $query      = $this->input->post('query');
        
        $rs         = getPurra()->search($query, $limit, $page);
        $rs         = @json_decode($rs);
        $this->data['rs'] = $rs;
        echo $this->getView("{$this->cmd}/loadmore_tab_promotion");
    }
    
    public function loadmore_tabActivity(){
        $page       = $this->input->post('page');
        $limit      = $this->input->post('limit');
        $query      = $this->input->post('query');
        
        $rs         = getPurra()->search($query, $limit, $page);
        $rs         = @json_decode($rs);
        $this->data['rs'] = $rs;
        echo $this->getView("{$this->cmd}/loadmore_tab_activity");
    }
    
    public function loadmore_tabLifestyle(){
        $page       = $this->input->post('page');
        $limit      = $this->input->post('limit');
        $query      = $this->input->post('query');
        
        $rs         = getPurra()->search($query, $limit, $page);
        $rs         = @json_decode($rs);
        $this->data['rs'] = $rs;
        echo $this->getView("{$this->cmd}/loadmore_tab_lifestyle");
    }
    
    
    
    private function getView($name){
        ob_start();
        $this->load->view($name, $this->data);
        return ob_get_clean();
    }
    
}