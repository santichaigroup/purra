<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promotion extends CI_Controller {
    
    public $cmd = "promotion";
    private $data = array();
    
    
    public function __construct() {
        parent::__construct();
        $this->data['body_class'] = "promotion";
        $this->data['mobileDetect'] = new Mobile_Detect();
    }
    
    
    public function index(){
        $this->showlist();
    }
    
    
    public function showlist(){
        $limit  = 10;
        $rs     = getPurra()->promotion( $limit );
        $rs     = @json_decode($rs);
        
//        $view   = "view_coming_soon";
//        if( $rs  &&  $rs->code==200 ){
//            $view = "{$this->cmd}/view_list";
//        }
        $view = "{$this->cmd}/view_list";
        
        $this->data['limit']    = $limit;
        $this->data['rs']       = $rs;
        $this->data['_BODY']    = $this->getView($view);
        $this->load->view('template', $this->data);
    }
    
    
    public function allPromotion(){
        $this->data['_BODY'] = $this->getView("{$this->cmd}/promotion_list");
//        $this->data['_BODY'] = $this->getView("view_coming_soon");
        $this->load->view('template', $this->data);
    }
    
    
    public function allActivities(){
        $this->data['_BODY'] = $this->getView("{$this->cmd}/activity_list");
//        $this->data['_BODY'] = $this->getView("view_coming_soon");
        $this->load->view('template', $this->data);
    }
    
    
    
    public function promotionDetail(){
        $this->data['body_class'] = "article";
        
        $id = $this->uri->segment(4, 0).'';
        
        $json     = getPurra()->promotionDetail($id);
        if( !$json ){
            redirect(_site_url($this->cmd) );
            return;
        }
        $json     = @json_decode($json);
        if( $json->data->quantity<0 ){
            redirect(_site_url($this->cmd) );
            return;
        }
        
        $content = $json->data->promotion[0];
        
        $title  = $content->title;
        $desc   = utf8_substr(strip_tags($content->long_des), 0, 150);
        $image  = $content->thumb_desktop;
        
        $title  = str_replace("'", "", str_replace('"', '', $title));
        $desc   = str_replace("'", "", str_replace('"', '', $desc));
        $txt    = ''
                . '<meta property="og:url" content="'. current_url().'" /> '
                . '<meta property="og:type"          content="website" /> '
                . '<meta property="og:title"         content="'. $title.'" /> '
                . '<meta property="og:description"   content="'. $desc.'" /> '
                . '<meta property="og:image"         content="'. $image.'" />'
                . '';
        $this->data['_TITLE'] = $title;
        $this->data['_DESC'] = $desc;
        $this->data['_IMAGE'] = $image;
        $this->data['_META_OG'] = $txt;
        $this->data['content'] = $content;
        $this->data['_DESCRIPTION'] = @$content->description ? $content->description : "";
        $this->data['_KEYWORD'] = @$content->keyword ? $content->keyword : "";
        
        $this->data['_BODY'] = $this->getView("{$this->cmd}/view_detail_promotion");
        $this->load->view('template', $this->data);
    }
    
    /*
    public function detail(){
        $this->data['body_class'] = "article";
        
        $id = $this->uri->segment(4, 0).'';
        
        $json     = getPurra()->promotionActivityDetail($id);
        if( !$json ){
            redirect(_site_url($this->cmd) );
            return;
        }
        $json     = @json_decode($json);
        if( $json->data->quantity<0 ){
            redirect(_site_url($this->cmd) );
            return;
        }
        
        $content = $json->data->promotion[0];
        
        $title  = $content->name;
        $desc   = utf8_substr(strip_tags($content->long_des), 0, 150);
        $image  = $content->thumb_desktop;
        
        $title  = str_replace("'", "", str_replace('"', '', $title));
        $desc   = str_replace("'", "", str_replace('"', '', $desc));
        $txt    = ''
                . '<meta property="og:url" content="'. current_url().'" /> '
                . '<meta property="og:type"          content="website" /> '
                . '<meta property="og:title"         content="'. $title.'" /> '
                . '<meta property="og:description"   content="'. $desc.'" /> '
                . '<meta property="og:image"         content="'. $image.'" />'
                . '';
        $this->data['_TITLE'] = $title;
        $this->data['_DESC'] = $desc;
        $this->data['_IMAGE'] = $image;
        $this->data['_META_OG'] = $txt;
        $this->data['content'] = $content;
        
        $this->data['_BODY'] = $this->getView("{$this->cmd}/view_detail_promotion");
        $this->load->view('template', $this->data);
    }
     * 
     */
    
    
    
    public function activityDetail(){
        $this->data['body_class'] = "article";
        
        $id = $this->uri->segment(4, 0).'';
        
        $json     = getPurra()->activityDetail($id);
        if( !$json ){
            redirect(_site_url($this->cmd) );
            return;
        }
        $json     = @json_decode($json);
        if( $json->data->quantity<0 ){
            redirect(_site_url($this->cmd) );
            return;
        }
        
        $content = $json->data->activity[0];
        
        $title  = $content->title;
        $desc   = utf8_substr(strip_tags($content->long_des), 0, 150);
        $image  = $content->thumb_desktop;
        
        $title  = str_replace("'", "", str_replace('"', '', $title));
        $desc   = str_replace("'", "", str_replace('"', '', $desc));
        $txt    = ''
                . '<meta property="og:url" content="'. current_url().'" /> '
                . '<meta property="og:type"          content="website" /> '
                . '<meta property="og:title"         content="'. $title.'" /> '
                . '<meta property="og:description"   content="'. $desc.'" /> '
                . '<meta property="og:image"         content="'. $image.'" />'
                . '';
        $this->data['_TITLE'] = $title;
        $this->data['_DESC'] = $desc;
        $this->data['_IMAGE'] = $image;
        $this->data['_META_OG'] = $txt;
        $this->data['content'] = $content;
        $this->data['_DESCRIPTION'] = @$content->description ? $content->description : "";
        $this->data['_KEYWORD'] = @$content->keyword ? $content->keyword : "";
        
        $this->data['_BODY'] = $this->getView("{$this->cmd}/view_detail_activity");
        $this->load->view('template', $this->data);
    }
    
    
    
    public function loadmore(){
        $page       = $this->input->post('page');
        $limit      = $this->input->post('limit');
        
        $rs         = getPurra()->promotion($limit, $page);
        $rs         = @json_decode($rs);
        $this->data['rs'] = $rs;
        echo $this->getView("promotion/promotion_load_more");
    }
    
    
    public function loadmorePromotion(){
        $page       = $this->input->post('page');
        $limit      = $this->input->post('limit');
        
        $rs         = getPurra()->promotion($limit, $page);
        $rs         = @json_decode($rs);
        $this->data['rs'] = $rs;
        echo $this->getView("promotion/promotion_load_more");
    }
    
    
    public function loadmorePromotionActivity(){
        $page       = $this->input->post('page');
        $limit      = $this->input->post('limit');
        
        $rs         = getPurra()->promotionActivity($limit, $page);
        $rs         = @json_decode($rs);
        $this->data['rs'] = $rs;
        echo $this->getView("promotion/promotion_activity_load_more");
    }
    
    
    public function loadmoreActivity(){
        $page       = $this->input->post('page');
        $limit      = $this->input->post('limit');
        
        $rs         = getPurra()->activity($limit, $page);
        $rs         = @json_decode($rs);
        $this->data['rs'] = $rs;
        echo $this->getView("promotion/activity_load_more");
    }
    
    
    
    private function getView($name){
        ob_start();
        $this->load->view($name, $this->data);
        return ob_get_clean();
    }
    
}