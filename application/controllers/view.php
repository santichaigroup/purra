<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->data['mobileDetect'] = new Mobile_Detect();
    }
    
   public $data = array();
    
    public function facebookFanpagePlugin(){
        $width      = @$_REQUEST['width']   ? $_REQUEST['width'] : 330;
        $height     = @$_REQUEST['height']  ? $_REQUEST['height'] : 315;
        $this->data['width'] = $width;
        $this->data['height'] = $height;
        echo $this->getView("include/facebook_fanpage_plugin");
    }
    
    
    
    
    public function vdoFromYoutubeLayout(){
        $this->data['rs'] = $this->vdoFromYoutubeChannelId();
        echo $this->getView("youtube/vdo_list");
    }
    
    
    
    
    const YOUTUBE_API_KEY = "AIzaSyCJ3ECij2oxGPuO0T_iDkb2y_c_oCQ2pJU";
    
    public function vdoFromYoutubeChannelId(){
        $social = getPurra()->social();
        $social = json_decode($social);
        
        $channelId          = $social->data->content->youtube;
        $url = "https://www.googleapis.com/youtube/v3/search?key=". View::YOUTUBE_API_KEY."&channelId={$channelId}&part=snippet,id&order=date&maxResults=50";
        $json = json_decode(file_get_contents($url));
        
        //exit(json_encode($json) );
        
        $videos = array();
        $channels = array();
        $rs_len = 0;
        
        if( $json ){
            $rs_len = count($json->items);
            for( $i=0;  $i<$rs_len;  $i++ ){
                $item = $json->items[$i];
                switch( strtolower($item->id->kind) ){
                    case 'youtube#video' : {
                        $videos[count($videos)] = array(
                            "id"                => $item->id->videoId,
                            "title"             => $item->snippet->title,
                            "description"       => $item->snippet->description,
                            "thumbnail_default" => $item->snippet->thumbnails->default->url,
                            "thumbnail_medium"  => $item->snippet->thumbnails->medium->url,
                            "thumbnail_high"    => $item->snippet->thumbnails->high->url
                        );
                    } break;
                    case 'youtube#channel' : {
                        $channels[count($channels)] = array(
                            "id"                => $item->id->channelId,
                            "title"             => $item->snippet->title,
                            "description"       => $item->snippet->description,
                            "thumbnail_default" => $item->snippet->thumbnails->default->url,
                            "thumbnail_medium"  => $item->snippet->thumbnails->medium->url,
                            "thumbnail_high"    => $item->snippet->thumbnails->high->url
                        );
                    } break;
                }
            }
        }

        $output     = array(
            'data'      => array(
                'vdo'       => $videos,
                'channel'   => $channels
            )
        );
        
        //echo json_encode($output);
        
        return $output;
    }
    
    
    public function instagram(){
        $this->data['rs'] = getPurra()->instagram();
        echo $this->getView("include/view_detail_social_tab_ig");
    }
    
    
    
    private function getView($name){
        ob_start();
        $this->load->view($name, $this->data);
        return ob_get_clean();
    }
    
}