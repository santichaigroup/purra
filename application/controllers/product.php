<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {
    
    public $cmd = "product";
    private $data = array();
    
    
    public function __construct() {
        parent::__construct();
        
        $this->data['body_class'] = "product";
        $this->data['mobileDetect'] = new Mobile_Detect();
    }
    
    
    public function index(){
        $content        = getPurra()->product();
        $content        = @json_decode($content);
        
        $this->data['_TITLE']   = @$content->data->product[0]->title ? $content->data->product[0]->title : "";
        $this->data['_DESC']    = @$content->data->product[0]->description ? $content->data->product[0]->description : "";
        $this->data['_KEYWORD'] = @$content->data->product[0]->keyword ? $content->data->product[0]->keyword : "";
        
        $this->data['_BODY']    = $this->getView("{$this->cmd}/view_main");
        $this->load->view('template', $this->data);
    }
    
    
    private function getView($name){
        ob_start();
        $this->load->view($name, $this->data);
        return ob_get_clean();
    }
    
}