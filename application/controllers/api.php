<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class API extends CI_Controller {
    
   
    
    public function newsLetter(){
        $email      = $this->input->post("newsleter_email");
        $output     = array();
        if( $email!="" ){
            $json    = getPurra()->newsLetter($email);
            $json    = json_decode($json);
            $output     = $json;
        }else{
            $output['code'] = 400;
            switch(getLang() ){
                case 'th' :
                    $output['message'] = "อีเมล์ไม่ถูกต้อง กรุณากรอกใหม่อีกครั้ง";
                    break;
                case 'en' :
                default :
                    //$output['message'] = "Email is invalid format";
                    $output['message'] = "อีเมล์ไม่ถูกต้อง กรุณากรอกใหม่อีกครั้ง";
                    break;
            }
        }
        
        echo json_encode($output);
        exit();
    }
    
    
    
    public function contact(){
        $output = array('status' => false);
        
        $capchaResponse     = $this->input->post('g-recaptcha-response');
        if( $capchaResponse=="" ){
            exit(json_encode($output) );
        }
        $data = array(
            'secret' => reCAPTCHA_SECRET_KEY,
            'response' => $capchaResponse,
            'remoteip' => $this->input->ip_address()
        );
        $validateCapcha = getPurra()->post('https://www.google.com/recaptcha/api/siteverify', $data);
		//pre($data);
		//exit($validateCapcha);
        $validateCapcha = @json_decode($validateCapcha);
        if( !$validateCapcha  ||  !$validateCapcha->success ){
            // Invalid capcha
            exit( json_encode($output) );
        }
        
        $type       = $this->input->post('type');
        $subject    = $this->input->post('subject');
        $name       = $this->input->post('name');
        $email      = $this->input->post('email');
        $phone      = $this->input->post('phone');
        $msg        = $this->input->post('message');
        $result = getPurra()->contact($type, $subject, $name, $email, $phone, $msg);
        $result = @json_decode($result);
        
        
        if( $result  &&  $result->code==200 ){
            $output['status'] = true;
        }
        
        echo json_encode($output);
    }
    
    
    
    
    
    
    public function test(){
        echo getPurra()->header();
    }
    
    public function testFooter(){
        echo getPurra()->footer();
    }
    
    
}