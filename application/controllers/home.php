<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
    public $cmd = "home";
    private $data = array();
    
    public function __construct() {
        parent::__construct();
        $this->data['body_class'] = "home";
        $this->data['mobileDetect'] = new Mobile_Detect();
    }
    
    
    public function index(){
        $this->data['_BODY'] = $this->getView("home/view_main");
        $this->load->view('template', $this->data);
    }
    
    
    private function getView($name){
        ob_start();
        $this->load->view($name, $this->data);
        return ob_get_clean();
    }
    
}