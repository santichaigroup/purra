<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->data['mobileDetect'] = new Mobile_Detect();
    }
    
    public function changeLanguage(){
        $method  = "location";
        $http_response_header = 301;
        
        $lang = strtolower($this->uri->segment(3));
        switch( $lang ){
            case 'en' :
            case 'th' :
                break;
            default : 
                $lang = "th";
                break;
        }
        
        $languages = @$_ENV['language_allowed'];
        if( !$languages ){
            $languages = array( 'th', 'en' );
        }
        
        setLang($lang);
        /*
        echo "\$this->agent->is_referral() : true";
        echo "<br />";
        echo "Refer URL: {$referUrl}";
        exit();
         * 
         */
        
        if ($this->agent->is_referral()){
            if( @$_GET['refer'] ){
                $referUrl = "{$lang}/{$_GET['refer']}";
            }else{
                $referUrl   = str_replace(base_url(), '', $this->agent->referrer());
                $_arr       = explode('/', $referUrl);
                if(is_array($_arr)  && count($_arr)>1 ){
                    $referUrl   = "{$lang}/";
                    foreach( $_arr AS $_arr_item ){
                        if( array_search(strtolower($_arr_item), $languages)===false ){
                            $referUrl.="{$_arr_item}/";
                        }
                    }
                }else{
                    $referUrl = "{$lang}/{$referUrl}";
                }
            }
            redirect($referUrl, $method, $http_response_header);
        }else{
            redirect(site_url("{$lang}/home"), $method, $http_response_header );
        }
    }
    
}