/*----------------------------------------
	JavaScript
----------------------------------------*/
$(document).ready(function(){
		  
	
    //$('header').stickyNavbar();
	
	
    $('select').selectpicker();
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('select').selectpicker('mobile');
	}	
	
	
	$('#FormContact').validate();
	
	
    $(".navbar-toggle").click(function() {
		$("#open-search").removeClass('active');
		$("#close-search").hide();
		$(".form-search").slideUp();
    });
	
	/*
	$("#close-search").hide();
    $("#open-search").click(function() {
		$("#open-search").toggleClass('active');
		$(".form-search").slideToggle(300);
    });
    */


	$('#owl-social').owlCarousel({
		loop: true,
		margin: 10,
		padding: 0,
		nav: true,
		navText: false,
		dots: false,
		responsive:{
			0: {
				items: 3,
				margin: 8,
			},
			960: {
				items: 3,
				margin: 8,
			},
			1024: {
				items: 4,
				margin: 8,
			},
			1367: {
				items: 4
			}
		}
	});


	$('#owl-lifestyle, #owl-promotion').owlCarousel({
		stagePadding: 60,
		margin: 0,
		nav: false,
		navText: false,
		responsive:{
			0:{
				items:2,
				margin: 20,
			},
			960:{
				items:3
			}
		}
	})


	$(".fancybox").fancybox({
		maxWidth	: '96%',
		maxHeight	: '96%',
		padding		: 0,
		fitToView	: false,
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers  	: {
			title:  null
		}
	});

	
    $(".scrollto > a[href^='#']").click(function(event) {
        var target = $($(this).attr("href"));
        event.preventDefault();
        $("html, body").animate({
            scrollTop: target.offset().top-50
        }, 500);
    });   


    $(".move_up").click(function() {
        $('html, body').animate({
            scrollTop: $("html").offset().top
        }, 400);
    });

		
    $(".nav-mobile ul ul").css({"height": "0"});
    $(".nav-mobile").on("click", "a", function () {
		
		$('.nav-mobile li').removeClass('active');
		
        if ($(this).next().css("height") != "0px") {
            $(this).next().css({
                "height": "0"
            });
			$('.nav-mobile li').removeClass('active');
        } else {
            $("ul ul").css({
                "height": "0"
            });
            var parent = $(this).parent();
            var element = $(this).next().clone().css({
                "height": "auto"
            }).appendTo(".nav-mobile");
            var height = element.css("height");
            element.remove();

            $(this).next().css({
                "height": height
            });
			$(this).parent().addClass('active');
        }
    });	
	

	function setModalMaxHeight(element) {
		this.$element = $(element);
		var dialogMargin = $(window).width() > 767 ? 122 : 82;
		var contentHeight = $(window).height() - dialogMargin;
		var headerHeight = this.$element.find('.modal-header').outerHeight() || 2;
		var footerHeight = this.$element.find('.modal-footer').outerHeight() || 2;
		var maxHeight = contentHeight - (headerHeight + footerHeight);
	
		this.$element.find('.modal-content').css({
			'overflow': 'hidden'
		});
	
		this.$element.find('.modal-body').css({
			'max-height': maxHeight,
				'overflow-y': 'auto'
		});
	}
	
	$('.modal').on('show.bs.modal', function () {
		$(this).show();
		setModalMaxHeight(this);
	});
	
	$(window).resize(function () {
		if ($('.modal.in').length == 1) {
			setModalMaxHeight($('.modal.in'));
		}
	});

	
	window.sr= new scrollReveal({
		move: 		'15px',
		over: 		'0.6s',
		scale: 		{ direction: 'up', power: '0' },
		opacity:	1,
		mobile:   	false,
		reset: 		true,
	});
	
		
    var md = new MobileDetect(window.navigator.userAgent);
    if (md.phone()) {
		document.getElementById("viewport").setAttribute("content", "width=640, user-scalable=no");

//		size_li = $("#list-items li").size();
//		x=5;
//		if(size_li<=x){
//			$('#loadMore').hide();
//		}
//		$('#list-items li:lt('+x+')').show();
//		$('#loadMore').click(function () {
//			x= (x+2 <= size_li) ? x+2 : size_li;
//			$('#list-items li:lt('+x+')').slideDown();
//		});

    }else if (md.tablet()) {
        document.getElementById("viewport").setAttribute("content", "width=960, user-scalable=no");

//        size_li = $("#list-items li").size();
//        x=10;
//        if(size_li<=x){
//                $('#loadMore').hide();
//        }
//        $('#list-items li:lt('+x+')').show();
//        $('#loadMore').click(function () {
//                x= (x+4 <= size_li) ? x+4 : size_li;
//                $('#list-items li:lt('+x+')').slideDown();
//        });

    }else{
		
//            size_li = $("#list-items li").size();
//            x=10;
//            if(size_li<=x){
//                    $('#loadMore').hide();
//            }
//            $('#list-items li:lt('+x+')').show();
//            $('#loadMore').click(function () {
//                    x= (x+4 <= size_li) ? x+4 : size_li;
//                    $('#list-items li:lt('+x+')').slideDown();
//            });

    }

	
});
